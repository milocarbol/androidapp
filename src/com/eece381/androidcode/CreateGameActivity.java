package com.eece381.androidcode;

import com.eece381.androidcode.communication.IncomingPackageResponse;
import com.eece381.androidcode.communication.OutgoingPackage;
import com.eece381.androidcode.communication.ServerCommunicator;
import com.eece381.androidcode.communication.SocketCommander;
import com.eece381.androidcode.communication.TCPReader;
import com.eece381.androidcode.gui.GameView;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

public class CreateGameActivity extends Activity {
	
	private final int waitTime = 10;
	private final int gameTime = 60;
	
	private GameView gameView;
	private SocketCommander socketCommander;
	private ServerCommunicator serverCommunicator;
	private TCPReader tcpReader;

	private TextView errorMsg;
	private Spinner mapSpinner;
	private ImageView mapPreview;
	private Button startGameBtn;
	
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.create_game_menu);
        
        socketCommander = SocketCommander.getSocketCommander();
        serverCommunicator = ServerCommunicator.getServerCommunicator(socketCommander);
		tcpReader = TCPReader.getTCPReader(serverCommunicator, socketCommander);
        
        startGameBtn = (Button) findViewById(R.id.startBtn);
        errorMsg = (TextView) findViewById(R.id.errorMessageCreate);
        mapSpinner = (Spinner) findViewById(R.id.mapSpinner);
        mapPreview = (ImageView) findViewById(R.id.mapPreview);
		
		startGameBtn.setOnClickListener(new View.OnClickListener() {
       	 
            public void onClick(View arg0) {
        		int map = mapSpinner.getSelectedItemPosition() + 1;
        		IncomingPackageResponse response = createGame(map,waitTime,gameTime);
            	if(response != null && response.getMap() > 0) {
            		gameView = new GameView(CreateGameActivity.this, serverCommunicator, tcpReader, response.getMap(), waitTime, gameTime, true, response.getId());
            		setContentView(gameView);
            	}else{
            		errorMsg.setTextColor(Color.RED);
            		if (response == null)
            			errorMsg.setText("Server unavailable.");
            		else
            			errorMsg.setText("Game already in progress (map" + response.getMap() + ").");
            	}
            }
        });
		
        mapSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
        	@Override
        	public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
        		int map = mapSpinner.getSelectedItemPosition() + 1;
        		switch (map) {
        			case 1:
        				mapPreview.setImageResource(R.drawable.mappreview);
        				break;
        			case 2:
        				mapPreview.setImageResource(R.drawable.map2preview);
        				break;
        			case 3:
        				mapPreview.setImageResource(R.drawable.map3preview);
        				break;
        			case 4:
        				mapPreview.setImageResource(R.drawable.map4);
        				break;
    				default:
    					mapPreview.setImageResource(R.drawable.mappreview);
        		}
        	}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				
			}
        });
        
	}
	
	private IncomingPackageResponse createGame(int map, int countdownTime, int gameTime){
		serverCommunicator.send(new OutgoingPackage(map, countdownTime, gameTime));
    	int startTime = (int)System.currentTimeMillis();
    	IncomingPackageResponse response = null;
    	while (response == null && (int)System.currentTimeMillis() - startTime < ServerCommunicator.TIMEOUT){
    		response = serverCommunicator.getResponse();
    	}
    	return response;
	}
}
