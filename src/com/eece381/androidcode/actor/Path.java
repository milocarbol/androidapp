package com.eece381.androidcode.actor;

import java.util.LinkedList;
import java.util.List;

import android.graphics.Point;

/**
 * Path of cells.
 * @author Milo
 */
public class Path {

	/** The steps along the path **/
	List<Point> steps = new LinkedList<Point>();
	
	/** Flag to indicate whether or not to allow the player to continue **/
	private boolean canContinue = true;
	
	int nextStep = 0;
	
	/**
	 * Adds a step to the list of steps.
	 * @param step - The step to add.
	 */
	public void addStep(Point step) { steps.add(step); }
	
	/**
	 * Gets the next step in the path and updates the state.
	 * @return the next step.
	 */
	public Point takeNextStep() {
		if (nextStep < steps.size() - 1)
			return steps.get(nextStep++);
		else
			return steps.get(nextStep);
	}
	
	/** @return checks the next step in the path **/
	public Point checkNextStep() { return steps.get(nextStep); }
	
	/** @return the last step in the path **/
	public Point lastStep() { return steps.get(steps.size() - 1); }
	
	/**
	 * Deletes the first step.
	 * DO NOT call this outside of a pathfinder object!!!
	 */
	public void removeStart() {
		if (nextStep != 0) throw new RuntimeException("Start of path deleted after use.");
		if (steps.size() > 0)
			steps.remove(0);
	}
	
	/** @return the length of the path **/
	public int size() { return steps.size(); }
	
	/**
	 * stops the player from continuing on the path
	 */
	
	public void stop() {
		canContinue = false;
	}
	
	/**
	 * 
	 * @return - whether or not the player can continue
	 */
	public boolean canContinue() {
		return canContinue && (steps.size() > 0);
	}
}
