package com.eece381.androidcode.actor;

public interface IPlayerManager {

	/**
	 * Updates all players.
	 */
	public void updatePlayers();
	
	/**
	 * Adds a player to the controlled players.
	 * @param player - The player to add.
	 */
	public void addPlayer(Player player);
}
