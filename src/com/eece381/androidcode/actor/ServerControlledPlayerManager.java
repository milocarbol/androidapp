package com.eece381.androidcode.actor;

import java.util.LinkedList;
import java.util.List;

import android.graphics.Point;

import com.eece381.androidcode.communication.IncomingPackagePlayer;
import com.eece381.androidcode.communication.OutgoingPackage;
import com.eece381.androidcode.communication.ServerCommunicator;

/**
 * Server-controlled player manager. Fixes player inconsistencies.
 * @author Milo
 */
public class ServerControlledPlayerManager implements IPlayerManager {
	
	/** The list of all players in the game **/
	private List<Player> players = new LinkedList<Player>();
	
	/** The last location tapped by the player **/
	private Point playerTarget = null;
	
	/** The server communicator **/
	private ServerCommunicator communicator;
	
	private int counter = 0;
	
	/**
	 * Creates a new ServerControlledPlayerManager with no players.
	 * @param communicator - The server communicator.
	 */
	public ServerControlledPlayerManager(ServerCommunicator communicator) {
		this.communicator = communicator;
	}
	
	/**
	 * Creates a new ServerControlledPlayerManager with a given player.
	 * @param player - The player to start the game with.
	 * @param communicator - The server communicator.
	 */
	public ServerControlledPlayerManager(Player player, ServerCommunicator communicator) {
		this.players.add(player);
		this.communicator = communicator;
	}
	
	/**
	 * Creates a new ServerControlledPlayerManager with a given list of players.
	 * @param players - The players to start the game with.
	 * @param communicator - The server communicator.
	 */
	public ServerControlledPlayerManager(List<Player> players, ServerCommunicator communicator) {
		this.players.addAll(players);
		this.communicator = communicator;
	}
	
	@Override
	public void addPlayer(Player player) {
		for (Player p : players)
			if (player.ID == p.ID)
				return;
		players.add(player);
	}
	
	/**
	 * Sets the human player's target position.
	 * @param playerTarget - The location that was tapped by the player.
	 */
	public void tap(Point playerTarget) {
		this.playerTarget = playerTarget;
	}
	
	@Override
	public void updatePlayers() {
	}
	
	public List<Player> getPlayers() { return players; }
	
	public void updatePlayers(IncomingPackagePlayer incoming) {
		for (Player player : players) {
			if (incoming != null) {
				
				if (!player.isLocal() || (incoming.serverIsOverriding() && player.isLocal())) {
					Point actualPosition = incoming.getPlayerLocation(player.ID);
					Point actualActivated = incoming.getPlayerActivated(player.ID);
					int powerups = incoming.getPlayerPowerups(player.ID);
					
					if (hasSpeedBoost(powerups))
						player.setSpeed(Player.highSpeed);
					else
						player.setSpeed(Player.normalSpeed);
					if (hasXRayVision(powerups))
						player.setHasXRayVision(true);
					else
						player.setHasXRayVision(false);
					
					player.setScore(incoming.getPlayerScore(player.ID));
					
					if (!player.getPosition().equals(actualPosition)) {
						player.setPosition(actualPosition);
						player.clearTick();
					}
					if (!player.getActivatedPosition().equals(actualActivated)) {
						player.activate(actualActivated);
						player.moveTowards(actualActivated);
					}
				}
			}
			if (player.isLocal() && playerTarget != null) {
				playerTarget = player.moveTowards(playerTarget);
				communicator.updateOutgoingPlayer(new OutgoingPackage(player));
			}
		}
	}
	
	/** @return the number of players managed **/
	public int numberOfPlayers() { return players.size(); }
	
	private boolean hasSpeedBoost(int powerups) {
		return powerups == 1 || powerups == 3;
	}
	
	private boolean hasXRayVision(int powerups) {
		return powerups == 2 || powerups == 3;
	}
}
