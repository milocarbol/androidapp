package com.eece381.androidcode.actor;

import java.util.LinkedList;
import java.util.List;


/**
 * Controls player movement.
 * @author Milo
 */
public class PlayerMover {

	/** The list of players to handle **/
	private List<Player> players;
	
	/**
	 * Creates a new player mover.
	 * @param players - The players to control.
	 */
	public PlayerMover(List<Player> players) {
		this.players = new LinkedList<Player>();
		players.addAll(players);
	}
	
	/**
	 * Initializes the list of players to one player
	 * @param player - the first player added
	 */
	public PlayerMover(Player player) {
		players = new LinkedList<Player>();
		players.add(player);
	}

	/**
	 * Adds a new player to the mover.
	 * @param player - the player to add.
	 */
	public void addPlayer(Player player) {
		for (Player p : players)
			if (player.ID == p.ID)
				return;
		players.add(player);
	}
	
	/**
	 * Moves all players.
	 */
	public void movePlayers() {
		for (Player player : players)
			player.move();
	}
	
	/** @return the number of players managed **/
	public int numberOfPlayers() { return players.size(); }
}
