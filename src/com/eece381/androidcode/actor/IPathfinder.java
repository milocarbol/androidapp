package com.eece381.androidcode.actor;

import android.graphics.Point;

/**
 * Computes a path.
 * @author Milo
 */
public interface IPathfinder {
	
	/**
	 * Generates a path.
	 * @param start - The start point.
	 * @param end - The end point.
	 * @return the path to follow from start to end.
	 */
	public Path generatePath(Point start, Point end);
}
