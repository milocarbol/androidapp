package com.eece381.androidcode.actor;

import android.graphics.Point;

/**
 * Generates a path based on Bresenham's line algorithm.
 * @author Milo
 */
public class LinearPathfinder implements IPathfinder {
	
	@Override
	public Path generatePath(Point start, Point end) {
		Path path = new Path();
		Point current = new Point(start);
		
		int dx = Math.abs(end.x - start.x);
		int dy = Math.abs(end.y - start.y);
		
		int sx = (start.x < end.x) ? 1 : -1;
		int sy = (start.y < end.y) ? 1 : -1;
		
		int err = dx - dy;
		
		while (true) {
			path.addStep(new Point(current));
			
			if (current.equals(end)) break;
			
			int e2 = 2*err;
			
			if (e2 > -dy) {
				err = err - dy;
				current.x += sx;
			}
			
			if (e2 < dx) {
				err = err + dx;
				current.y += sy;
			}
		}
		
		path.removeStart();
		return path;
	}
}
