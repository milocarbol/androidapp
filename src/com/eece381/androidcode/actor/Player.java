package com.eece381.androidcode.actor;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.media.MediaPlayer;
import android.util.Log;

import com.eece381.androidcode.R;
import com.eece381.androidcode.game.GameBoard;
import com.eece381.androidcode.gui.GameView.Images;

/**
 * Player object.
 * @author Milo
 */
public class Player {

	/** Normal Speed **/
	public static final int normalSpeed = 15;
	
	/** High Speed **/
	public static final int highSpeed = 10;
	
	/** This Player's ID **/
	public final int ID;
	
	/** Whether or not can see through walls **/
	private boolean hasXRayVision;
	
	/** Counter keeping track of XRay Duration **/
	private int powerUpXRayCounter;
	
	/** Dirty flag for XRay to determine if needs to update orientation **/
	private boolean xRayToggle;
	
	/** Coutner keeping track of Speed Boost Duration **/
	private int powerUpSpeedCounter;
	
	/** Dirty flag for speedBoost to determine if needs to update orientation **/
	private boolean speedBoostToggle;
	
	/** Whether the player has the key or not **/
	private boolean hasKey;
	
	/** This Player's score **/
	private int score;
	
	/** This Player's current position **/
	private Point position;
	
	/** The point this Player is currently moving towards **/
	private Point target;
	
	/** The point this Player is currently activating **/
	private Point activated;
	
	/** The speed at which this Player moves **/
	private int speed;
	
	/** Whether or not this player is human **/
	private boolean isLocal;
	
	/** The path finder **/
	private IPathfinder pathfinder;
	
	/** The journey this player is currently on **/
	private Path currentPath;
	
	/** The gameboard containing cells to move to **/
	private GameBoard gameBoard;
	
	/** Contains bitmap images **/
	private final Images images;
	
	/** Current index of players orientation image **/
	private int orientationIndex = 0;
	
	/** Number of elapsed ticks since the player activated the next space in its path **/
	private int elapsedTicks = 0;
	
	private Context callingActivity;
	
	private MediaPlayer backgroundMusic;
	
	private boolean hasActivated = false;
	
	/**
	 * Creates a new Player object.
	 * @param id - The ID of this player.
	 * @param initialPosition - The position at which this Player will start.
	 * @param speed - The speed of this Player.
	 * @param isLocal - Whether or not this Player is locally controlled.
	 */
	public Player(int id, Point initialPosition, boolean isLocal, IPathfinder pathfinder, GameBoard gameBoard, Images images, Context callingActivity) {
		this.ID = id;
		this.position = initialPosition;
		this.target = initialPosition;
		this.activated = initialPosition;
		this.speed = normalSpeed;
		this.isLocal = isLocal;
		this.pathfinder = pathfinder;
		this.gameBoard = gameBoard;
		this.images = images;
		this.currentPath = this.pathfinder.generatePath(position, target);
		this.hasXRayVision = false;
		this.callingActivity = callingActivity;
		if(isLocal){
			backgroundMusic = MediaPlayer.create(callingActivity, R.raw.main_music);
			backgroundMusic.setLooping(true);
			backgroundMusic.start();
		}
	}
	
	/**
	 * Teleports this Player to a new position.
	 * @param newPosition - The new position for the Player.
	 */
	public void setPosition(Point newPosition) {
		position = newPosition;
	}
	
	/**
	 * If the player has finished one cell of movement, move to it and activate the next cell to move to.
	 * Otherwise, wait to finish the cell.
	 */
	public void move() {
		if (elapsedTicks == speed) {
			position = activated;
			if (currentPath.canContinue() && !position.equals(target)) {
				Point potentialNext = currentPath.checkNextStep();
				Log.d("path", potentialNext + " " + target);
				if(potentialNext.equals(target) && gameBoard.isInteractableElement(target)) {
					//Interact with active element
					gameBoard.interactWithCell(target, this);
					//Stop the player
					stopMoving();
				}
				else if (!gameBoard.isMovementBlocking(potentialNext))
					activate(potentialNext);
				else if (gameBoard.isMovementBlocking(potentialNext)){
					// Potential step is movement blocking
					// So slide in the direction we want to go.
					boolean targetIsDown = target.y - position.y > 0;
					boolean targetIsUp = target.y - position.y < 0;
					boolean targetIsRight = target.x - position.x > 0;
					boolean targetIsLeft = target.x - position.x < 0;
					Point newNext = new Point(position);
					
					boolean horizontalWasValid = false;
					if (targetIsRight) {
						newNext.x++;
						if (!gameBoard.isMovementBlocking(newNext)) {
							horizontalWasValid = true;
							activate(newNext);
							moveTowards(target);
						}
						else newNext.x--;
					}
					else if (targetIsLeft) {
						newNext.x--;
						if (!gameBoard.isMovementBlocking(newNext)) {
							horizontalWasValid = true;
							activate(newNext);
							moveTowards(target);
						}
						else newNext.x++;
					}
					if (!horizontalWasValid && targetIsUp) {
						newNext.y--;
						if (!gameBoard.isMovementBlocking(newNext)) {
							activate(newNext);
							moveTowards(target);
						}
						else newNext.y++;
					}
					else if (!horizontalWasValid && targetIsDown) {
						newNext.y++;
						if (!gameBoard.isMovementBlocking(newNext)) {
							activate(newNext);
							moveTowards(target);
						}
						else newNext.y--;
					}
				}
			}
			elapsedTicks = 0;
		}
		else {
			elapsedTicks++;
		}
		if(isLocal){
			decrementPowerUpCount();
			updateAudioTrack();
		}
	}
	
	/**
	 * Sets the location towards which this Player should begin moving.
	 * @param target - The target location.
	 */
	public Point moveTowards(Point target) {
		if(hasActivated){
			hasActivated = false;
			return position;
		}
		else if (!this.position.equals(target) && !this.activated.equals(target)) {
			if (!gameBoard.isMovementBlocking(activated))
				currentPath = pathfinder.generatePath(activated, target);
			else
				currentPath = pathfinder.generatePath(position, target);
			this.target = target;
			return target;
		}
		else {
			return this.target;
		}
	}
	
	/**
	 * Stops the player from completing their path (used for activation elements)
	 */
	private void stopMoving() {
		currentPath.stop();
		activated = position;
		target = position;
		hasActivated = true;
	}
	
	/**
	 * Gets the players (this) current orientation based on:
	 * direction (actived & position cells)
	 * hasXRayVision
	 * and hasSpeedBoost
	 * @return the Bitmap with the correct player orientation
	 */
	public Bitmap getPlayerOrientation(boolean forceOverride) {
		if(!activated.equals(position) || forceOverride){
			//If the player is moving update orientation image
			orientationIndex = 0;
			
			//Get picture
			orientationIndex = 16*(ID%3);
			
			//Get Y Direction
			if(activated.y >= position.y) {
				//Going down
				orientationIndex += 8;
			}
			
			//Get X Direction
			if(activated.x >= position.x) {
				//Going right
				orientationIndex += 4;
			}
			
			//Get Boots status
			if(hasSpeedBoost()) {
				//Has speed boost
				orientationIndex += 1;
			}
			
			//Get XRay status
			if(hasXRayVision) {
				//Has Xray vision
				orientationIndex += 2;
			}
		}else{
			if(speedBoostToggle) {
				//Boots status change
				if(hasSpeedBoost()) {
					//has speed boost
					orientationIndex += 1;
				}else{
					//doesnt have speed boost
					orientationIndex -= 1;
				}
			}
			if(xRayToggle){
				//XRay status change
				if(hasXRayVision) {
					//has XRay vision
					orientationIndex += 2;
				}else{
					orientationIndex -= 2;
				}
			}
		}
		
		speedBoostToggle = false;
		xRayToggle = false;
		
		try {
			return images.playerImages.get(orientationIndex);
		}
		catch (RuntimeException r) {
			throw new RuntimeException(images.playerImages.size() + " " + orientationIndex);
		}
	}
	
	private void decrementPowerUpCount(){
		if(powerUpXRayCounter == 0){
			setHasXRayVision(false);
		}else{
			powerUpXRayCounter--;
		}
		
		if(powerUpSpeedCounter == 0){
			setSpeed(normalSpeed);
		}else{
			powerUpSpeedCounter--;
		}
	}
	
	private void updateAudioTrack() {
		if(speedBoostToggle || xRayToggle){
			//Something just got picked up or dropped
			if(!hasSpeedBoost() && !getHasXRayVision()){
				backgroundMusic = MediaPlayer.create(callingActivity, R.raw.main_music);
				backgroundMusic.setLooping(true);
				backgroundMusic.start();
			}else{
				if((speedBoostToggle && !getHasXRayVision()) || (xRayToggle && !hasSpeedBoost()))
				{
					//fresh item pickup (no previous items)
					backgroundMusic = MediaPlayer.create(callingActivity, R.raw.power_ups);
					backgroundMusic.setLooping(true);
					backgroundMusic.start();
				}
			}
			
		}
		
	}
	
	/**
	 * Sets the activated location to a new adjacent location.
	 * @param activated - The new adjacent location to activate.
	 */
	public void activate(Point activated) {
		this.activated = activated;
	}
	
	/**
	 * 
	 * @param gains - value added to the score of player
	 */
	public void addScore(int gains) {
		this.score += gains;
	}
	
	public boolean hasSpeedBoost() {
		return (speed == highSpeed);
	}
	
	/**
	 * Sets this Player's speed to a new speed value.
	 * @param newSpeed - The new speed.
	 */
	public void setSpeed(int newSpeed) { 
		if(newSpeed != speed){
			speedBoostToggle = true;
		}
		speed = newSpeed; 
	}
	
	/**
	 * adds the speed boost power up duration (powerup times stack)
	 * @param delayCount - the number of game ticks which occur before you are reverted back to normal speed
	 */
	public void setSpeedBoostCounter(int delayCount) { this.powerUpSpeedCounter += delayCount; }
	
	/**
	 * Sets XRayVision
	 * @param hasXRayVision - true can see through walls, false cannot
	 */
	public void setHasXRayVision(boolean hasXRayVision) { 
		if(this.hasXRayVision != hasXRayVision) {
			xRayToggle = true;
		}
		this.hasXRayVision = hasXRayVision; 
	}
	
	/**
	 * Sets the score.
	 * @param score - the new score.
	 */
	public void setScore(int score) {
		this.score = score;
	}
	
	/**
	 * Sets elapsed ticks to 0.
	 */
	public void clearTick() { elapsedTicks = 0; }
	
	/**
	 * adds the XRay vision power up duration (powerup times stack)
	 * @param delayCount - the number of game ticks which occur before you are reverted to normal vision
	 */
	public void setXRayCounter(int delayCount) { this.powerUpXRayCounter += delayCount; }
	
	/**@return whether or not the player can see through walls **/
	public boolean getHasXRayVision() { return hasXRayVision; }
	
	/** @return whether the player has the key or not  **/
	public boolean getHasKey() { return hasKey; }
	
	/** Player picks up the key **/
	public void pickUpKey() { hasKey = true; }
	
	/** Player drops the key **/
	public void dropKey() { hasKey = false; }
	
	/** @return the current position of this Player **/
	public Point getPosition() { return position; }
	
	/** @return the target point towards which this Player is moving **/
	public Point getTargetLocation() { return target; }
	
	/** @return the adjacent point this Player is activating **/
	public Point getActivatedPosition() { return activated; }
	
	/** @return this Player's speed **/
	public int getSpeed() { return speed; }
	
	/** @return if this Player is locally controlled **/
	public boolean isLocal() { return isLocal; }
	
	/** @return the current tick count for the player **/
	public int getTick() { return elapsedTicks; }
	
	/** @return get the score of player **/
	public int getScore() { return score; }
	
	/** @return if the game should render the target position **/
	public boolean shouldDrawTarget() {
		return !position.equals(target) && !activated.equals(target);
	}
}
