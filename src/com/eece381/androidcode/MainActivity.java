package com.eece381.androidcode;

import java.util.Timer;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.eece381.androidcode.communication.IncomingPackageResponse;
import com.eece381.androidcode.communication.OutgoingPackage;
import com.eece381.androidcode.communication.ServerCommunicator;
import com.eece381.androidcode.communication.SocketCommander;
import com.eece381.androidcode.communication.TCPReader;
import com.eece381.androidcode.gui.GameView;

public class MainActivity extends Activity {
	
	private GameView gameView;
	private SocketCommander socketCommander;
	private ServerCommunicator serverCommunicator;
	private TCPReader tcpReader;
	private TextView errorMsg;
	public static Timer timer = new Timer();
	public static boolean runAllTasks = true;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        socketCommander = SocketCommander.getSocketCommander();
        serverCommunicator = ServerCommunicator.getServerCommunicator(socketCommander);
		tcpReader = TCPReader.getTCPReader(serverCommunicator, socketCommander);
	//	tcpReader.clearBuffer();
		if(runAllTasks)
			timer.scheduleAtFixedRate(tcpReader, 0, GameView.gameDelay/2);
		runAllTasks = true;
        
        
        Button joinBtn = (Button) findViewById(R.id.joinButton);
        Button createBtn = (Button) findViewById(R.id.createButton);
        errorMsg = (TextView) findViewById(R.id.errorMessageMain);
        
        // Binding Click event to Button
        joinBtn.setOnClickListener(new View.OnClickListener() {
 
            public void onClick(View arg0) {
            	IncomingPackageResponse response = joinGame();
            	if(response != null && response.getMap() > 0) {
            		gameView = new GameView(MainActivity.this, serverCommunicator, tcpReader, response.getMap(), response.getWaitTimeRemaining(), response.getGameTimeRemaining(), false, response.getId());
            		setContentView(gameView);
            	}else{
            		errorMsg.setTextColor(Color.RED);
            		if (response == null)
            			errorMsg.setText("Server unavailable.");
            		else
            			errorMsg.setText("No game in progress.");
            	}
            }
        });
        
        // Binding Click event to Button
        createBtn.setOnClickListener(new View.OnClickListener() {
 
            public void onClick(View arg0) {
            		//gameView = new GameView(MainActivity.this, serverCommunicator, tcpReader, map);
            		Intent intent = new Intent(MainActivity.this, CreateGameActivity.class);
            		startActivity(intent);
            }
        });
    }
    
    /**
     * Checks to see if a game is in progress
     * @return - true if game is in progress false otherwise
     */
    private IncomingPackageResponse joinGame() {
    	serverCommunicator.send(new OutgoingPackage(0,0,0));
    	int startTime = (int)System.currentTimeMillis();
    	IncomingPackageResponse response = null;
    	while ((response = serverCommunicator.getResponse()) == null && (int)System.currentTimeMillis() - startTime < ServerCommunicator.TIMEOUT);
		return response;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
    
}
