package com.eece381.androidcode;

import com.eece381.androidcode.communication.IncomingPackageResponse;
import com.eece381.androidcode.gui.GameView;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class EndGameActivity extends Activity {
	
	private Button mainMenuBtn;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
        setContentView(R.layout.end_game);
        

        TextView firstPlace = (TextView) findViewById(R.id.firstPlace);
        TextView secondPlace = (TextView) findViewById(R.id.secondPlace);
        TextView thirdPlace = (TextView) findViewById(R.id.thirdPlace);
        TextView fourthPlace = (TextView) findViewById(R.id.fourthPlace);
        
        Bundle extras = getIntent().getExtras();
        
        TextView[] placementWidgets = {firstPlace, secondPlace, thirdPlace, fourthPlace};
        String[] extraKeys = {"firstPlace","secondPlace","thirdPlace","fourthPlace"};
        String[] placementText = {"First Place: ", "Second Place: ", "Third Place: ", "Fourth Place: "};
        
        //Print Scores
        for(int i = 0; i < extras.getInt("numPlayers"); i++){
        	if(extras.getInt("localPlayersScore") == extras.getInt(extraKeys[i])){
        		//Local Players score
        		placementWidgets[i].setTextColor(Color.BLUE);
        	}else{
        		placementWidgets[i].setTextColor(Color.BLACK);
        	}
        	
        	placementWidgets[i].setText(placementText[i] + "" + extras.getInt(extraKeys[i]));
        }
        
        mainMenuBtn = (Button) findViewById(R.id.mainMenuButton);
        
        // Binding Click event to Button
        mainMenuBtn.setOnClickListener(new View.OnClickListener() {
 
            public void onClick(View arg0) {
            	Intent intent = new Intent(EndGameActivity.this, MainActivity.class);
        		startActivity(intent);
            }
        });
	}
	
	@Override
	public void onBackPressed() {
		
	}
}
