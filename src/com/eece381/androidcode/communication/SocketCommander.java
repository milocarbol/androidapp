package com.eece381.androidcode.communication;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.UnknownHostException;

import android.os.AsyncTask;
import android.util.Log;

/**
 * Handler for socket data.
 * @author Milo
 */
public class SocketCommander {

	/** The IP address for middleman **/
	private final String ip = "192.168.1.133";
	
	/** The port for middleman **/
	private final int port = 50002;
	
	/** The socket object **/
	private Socket socket = null;
	
	static private SocketCommander socketCommander = null;
	
	static public SocketCommander getSocketCommander(){
		if (socketCommander == null)
			socketCommander = new SocketCommander();
		return socketCommander;
	}
	
	private SocketCommander() {
		
	}
	
	/**
	 * Initializes a connection.
	 */
	public void openSocket() {
		Log.d("Socket", "Attempting to open socket.");
		if (socket != null && socket.isConnected() && !socket.isClosed()) {
			return;
		}
		new SocketConnect().execute((Void) null);
	}
	
	/**
	 * @return the socket's output stream.
	 * @throws IOException if the output stream is not available.
	 */
	public OutputStream getOutputStream() throws IOException { return socket.getOutputStream(); }
	
	/**
	 * @return the socket's input stream.
	 * @throws IOException if the input stream is not available.
	 */
	public InputStream getInputStream() throws IOException { return socket.getInputStream(); }
	
	/** @return if the socket is available to use. **/
	public boolean isActive() { return socket != null && socket.isConnected() && !socket.isClosed(); }
	
	/** @return the IP address to try to connect to. **/
	private String getTargetIP() { return ip; }
	
	/** @return the port to connect to. **/
	private int getTargetPort() { return port; }
	
	/**
	 * Does some socket shit we don't understand.
	 * @author Andrew
	 */
	public class SocketConnect extends AsyncTask<Void, Void, Socket> {

		/**
		 * Does some stuff.
		 */
		protected Socket doInBackground(Void... voids) {
			Socket s = null;
			String ip = getTargetIP();
			Integer port = getTargetPort();

			try {
				s = new Socket(ip, port);
			} catch (UnknownHostException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return s;
		}
		
		/**
		 * Does more stuff.
		 */
		protected void onPostExecute(Socket s) {
			socket = s;
		}
	}
}
