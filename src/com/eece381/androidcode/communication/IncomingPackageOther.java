package com.eece381.androidcode.communication;

import java.util.LinkedList;
import java.util.List;

import android.graphics.Point;

/**
 * Package received from the server. Contains special item information.
 * @author Milo
 */
public class IncomingPackageOther {

	/** Closed lockable door locations **/
	private List<Point> closedLockableDoors = new LinkedList<Point>();
	
	/** Open lockable door locations **/
	private List<Point> openLockableDoors = new LinkedList<Point>();
	
	/** Speed boost locations **/
	private List<Point> speedBoosts = new LinkedList<Point>();
	
	/** Goggle locations **/
	private List<Point> goggles = new LinkedList<Point>();
	
	/** Key locations **/
	private List<Point> keys = new LinkedList<Point>();
	
	private static final int closedLockableDoorCode = 0;
	private static final int openLockableDoorCode = 1;
	private static final int keyCode = 2;
	private static final int speedCode = 3;
	private static final int goggleCode = 4;
	
	/** The number of bytes per thing **/
	private static final int numberOfBytesPerThing = 3;
	
	/**
	 * Creates a new IncomingMessageDoor and populates it from bytes.
	 * @param bytes - the bytes.
	 */
	public IncomingPackageOther(byte[] bytes) {
		for (int i = 0; i <= bytes.length - numberOfBytesPerThing; i += numberOfBytesPerThing)
			if (bytes[i] == closedLockableDoorCode)
				closedLockableDoors.add(new Point(bytes[i+1], bytes[i+2]));
			else if (bytes[i] == openLockableDoorCode)
				openLockableDoors.add(new Point(bytes[i+1], bytes[i+2]));
			else if (bytes[i] == keyCode)
				keys.add(new Point(bytes[i+1], bytes[i+2]));
			else if (bytes[i] == speedCode)
				speedBoosts.add(new Point(bytes[i+1], bytes[i+2]));
			else if (bytes[i] == goggleCode)
				goggles.add(new Point(bytes[i+1], bytes[i+2]));
	}
	
	/** @return the list of closed lockable door locations. **/
	public List<Point> getClosedLockableDoorLocations() { return closedLockableDoors; }
	
	/** @return the list of open lockable door locations. **/
	public List<Point> getOpenLockableDoorLocations() { return openLockableDoors; }
	
	/** @return the list of key locations. **/
	public List<Point> getKeyLocations() { return keys; }
	
	/** @return the list of speed boost locations. **/
	public List<Point> getSpeedBoostLocations() { return speedBoosts; }
	
	/** @return the list of goggle locations. **/
	public List<Point> getGoggleLocations() { return goggles; }
}
