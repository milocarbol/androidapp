package com.eece381.androidcode.communication;

import java.io.IOException;

/**
 * Communicates with the server by listening for incoming packages and sending outgoing packages at an interval.
 * @author Andrew, Nigel, milo
 */
public class ServerCommunicator {
	
	public static final int TIMEOUT = 5000;
	
	/** The packages that come in from the server **/
	private IncomingPackagePlayer incomingPlayer;
	private IncomingPackageTreasure incomingCoin;
	private IncomingPackageTreasure incomingGem;
	private IncomingPackageTreasure incomingChest;
	private IncomingPackageDoor incomingDoor;
	private IncomingPackageOther incomingOther;
	private IncomingPackageResponse response;
	private IncomingPackageUpdate update;
	private IncomingPackageTimer incomingTimer;
	
	/** The package to be sent to the server **/
	private OutgoingPackage outgoingPlayer;
	
	private OutgoingPackage outgoingInteraction;
	
	/** The handler for connection socket data **/
	private SocketCommander	socketCommander;
	
	static private ServerCommunicator serverCommunicator;
	
	private static final int	messageTypePlayer = 0,
								messageTypeCoin = 1,
								messageTypeGem = 2,
								messageTypeChest = 3,
								messageTypeDoor = 4,
								messageTypeOther = 5,
								messageTypeResponse = 6,
								messageTypeUpdate = 7,
								messageTypeTimer = 8;
	
	static public ServerCommunicator getServerCommunicator(SocketCommander socketCommander){
		if(serverCommunicator == null)
			serverCommunicator = new ServerCommunicator(socketCommander);
		return serverCommunicator;
	}
	
	/**
	 * Creates a new ServerCommunicator object.
	 */
	private ServerCommunicator(SocketCommander socketCommander) {
		this.socketCommander = socketCommander;
		socketCommander.openSocket();
	}
	
	/**
	 * Updates the outgoing package if there are any differences between it and 'updates'.
	 * @param updates - The package containing updates to make.
	 */
	public synchronized void updateOutgoingPlayer(OutgoingPackage updates) {
		if (updates.equals(outgoingPlayer))
			return;
		outgoingPlayer = updates;
		send(outgoingPlayer);
	}
	
	public synchronized void setResponse(IncomingPackageResponse incoming) {
		response = incoming;
	}
	
	public synchronized IncomingPackageResponse getResponse() {
		IncomingPackageResponse temp = response;
		response = null;
		return temp;
	}
	
	public synchronized IncomingPackageUpdate getUpdate() { return update; }
	
	public synchronized IncomingPackageTimer getTimer() { 
		IncomingPackageTimer temp = incomingTimer;
		incomingTimer = null;
		return temp; }
	
	/**
	 * Updates the outgoing package if there are any differences between it and 'updates'.
	 * @param updates - The package containing updates to make.
	 */
	public synchronized void updateOutgoingInteraction(OutgoingPackage updates) {
		if (updates.equals(outgoingInteraction))
			return;
		outgoingInteraction = updates;
		send(outgoingInteraction);
	}
	
	/**
	 * Retreives the incoming package which contains the last updates from the server.
	 * @return the last received data package.
	 */
	public synchronized IncomingPackagePlayer getLastPlayer() {
		IncomingPackagePlayer temp = incomingPlayer;
		incomingPlayer = null;
		return temp;
	}
	
	/**
	 * Retreives the incoming package which contains the last updates from the server.
	 * @return the last received data package.
	 */
	public synchronized IncomingPackageTreasure getLastCoin() {
		IncomingPackageTreasure temp = incomingCoin;
		incomingCoin = null;
		return temp;
	}
	
	/**
	 * Retreives the incoming package which contains the last updates from the server.
	 * @return the last received data package.
	 */
	public synchronized IncomingPackageTreasure getLastGem() {
		IncomingPackageTreasure temp = incomingGem;
		incomingGem = null;
		return temp;
	}
	
	/**
	 * Retreives the incoming package which contains the last updates from the server.
	 * @return the last received data package.
	 */
	public synchronized IncomingPackageTreasure getLastChest() {
		IncomingPackageTreasure temp = incomingChest;
		incomingChest = null;
		return temp;
	}
	
	/**
	 * Retreives the incoming package which contains the last updates from the server.
	 * @return the last received data package.
	 */
	public synchronized IncomingPackageDoor getLastDoor() {
		IncomingPackageDoor temp = incomingDoor;
		incomingDoor = null;
		return temp;
	}
	
	/**
	 * Retreives the incoming package which contains the last updates from the server.
	 * @return the last received data package.
	 */
	public synchronized IncomingPackageOther getLastOther() {
		IncomingPackageOther temp = incomingOther;
		incomingOther = null;
		return temp;
	}
	
	/**
	 * Sends the outgoing package if one exists and the socket is open.
	 * @author Andrew
	 */
	public synchronized void send(OutgoingPackage outgoing) {
		if (outgoing != null && socketCommander.isActive()) {
			try {
				socketCommander.getOutputStream()
					.write(outgoing.bytesToSend, 0, outgoing.bytesToSend.length);
			}
			catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * Receives a server package and updates the local package.
	 * @param buffer - the bytes that were received.
	 */
	public synchronized void receive(byte[] buffer) {
		int messageType = buffer[0];
		byte[] data = new byte[buffer.length-2];
		for (int i = 0; i < data.length; i++)
			data[i] = buffer[i+2];
		switch (messageType) {
		case messageTypePlayer:
			incomingPlayer = new IncomingPackagePlayer(data);
			break;
		case messageTypeCoin:
			incomingCoin = new IncomingPackageTreasure(data);
			break;
		case messageTypeGem:
			incomingGem = new IncomingPackageTreasure(data);
			break;
		case messageTypeChest:
			incomingChest = new IncomingPackageTreasure(data);
			break;
		case messageTypeDoor:
			incomingDoor = new IncomingPackageDoor(data);
			break;
		case messageTypeOther:
			incomingOther = new IncomingPackageOther(data);
			break;
		case messageTypeResponse:
			response = new IncomingPackageResponse(data);
			break;
		case messageTypeUpdate:
			update = new IncomingPackageUpdate(data);
			break;
		case messageTypeTimer:
			incomingTimer = new IncomingPackageTimer(data);
			break;
		}
	}
}
