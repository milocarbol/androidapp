package com.eece381.androidcode.communication;

import java.util.LinkedList;
import java.util.List;

import android.graphics.Point;

/**
 * Package received from the server. Contains treasure information.
 * @author Milo
 */
public class IncomingPackageTreasure {

	/** Treasure locations **/
	private List<Point> treasures = new LinkedList<Point>();
	
	/** Number of bytes in a package per treasure **/
	private static final int numberOfBytesPerTreasure = 2;
	
	/**
	 * Creates a new IncomingMessageCoin and populates it from bytes.
	 * @param bytes - the bytes.
	 */
	public IncomingPackageTreasure(byte[] bytes) {
		for (int i = 0; i <= bytes.length - numberOfBytesPerTreasure; i += numberOfBytesPerTreasure)
			treasures.add(new Point(bytes[i], bytes[i+1]));
	}
	
	/** @return the list of treasure locations. **/
	public List<Point> getTreasureLocations() { return treasures; }
}
