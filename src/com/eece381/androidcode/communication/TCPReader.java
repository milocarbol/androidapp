package com.eece381.androidcode.communication;

import java.io.IOException;
import java.io.InputStream;
import java.util.LinkedList;
import java.util.List;
import java.util.TimerTask;

import com.eece381.androidcode.MainActivity;

import android.util.Log;

/**
 * Reads incoming packages from the server.
 * @author Andrew
 */
public class TCPReader extends TimerTask {
	
	/** The socket data **/
	private SocketCommander socketCommander;
	
	/** The server communicator object **/
	private ServerCommunicator serverCommunicator;
	
	private static TCPReader tcpReader;
	
	/** Buffer of messages **/
	private List<Byte> buffer = new LinkedList<Byte>();
	
	/** Start of message flag **/
	public static byte HEAD = (byte)0xff;
	
	/** End of message flag **/
	public static byte FOOT = (byte)0xfe;
	
	public static TCPReader getTCPReader(ServerCommunicator serverCommunicator, SocketCommander socketCommander) {
		if(tcpReader == null)
			tcpReader = new TCPReader(serverCommunicator, socketCommander);
		return tcpReader;
	}
	
	/**
	 * Creates a new TCP reader.
	 * @param serverCommunicator - The server communicator.
	 * @param socketCommander - The socket data.
	 */
	public TCPReader(ServerCommunicator serverCommunicator, SocketCommander socketCommander) {
		this.serverCommunicator = serverCommunicator;
		this.socketCommander = socketCommander;
	}
	
	public void clearBuffer(){
		buffer.clear();
	}
	
	/**
	 * Reads from the input buffer.
	 */
	public synchronized void run() {
		if(MainActivity.runAllTasks){
			if (socketCommander.isActive()) {
				try {
					InputStream in = socketCommander.getInputStream();
					
					int bytes_avail = in.available();
					if (bytes_avail > 0) {
						Log.d("communicate", "reading...");
						byte buf[] = new byte[bytes_avail];
						in.read(buf);
						for (int i = 0; i < buf.length; i++)
							buffer.add(buf[i]);
					}
					if (bufferIsValid()) {
						byte[] incomingMsg = getMessage();
						if (incomingMsg.length > 0)
							serverCommunicator.receive(incomingMsg);
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	/**
	 * Gets the latest full message from the buffer.
	 * @return the latest full message from the buffer.
	 */
	private synchronized byte[] getMessage() {
		String t = "";
		for (Byte b : buffer) {
			t += b;
			t += " ";
		}
		Log.d("communicate", t);
		if (buffer.get(0) == HEAD)
			buffer.remove(0);
		else throw new RuntimeException("Buffer was not cleaned properly.");
		
		int end;
		for (end = 0; end < buffer.size(); end++)
			if (buffer.get(end) == FOOT) break;
		byte[] buf = new byte[end];
		
		for (int i = 0; i < end; i++) {
			buf[i] = buffer.get(0);
			buffer.remove(0);
		}
		
		if (buffer.get(0) == FOOT)
			buffer.remove(0);
		else throw new RuntimeException("Full message was not collected.");
		
		return buf;
	}
	
	/** @return if the buffer is valid **/
	private synchronized boolean bufferIsValid() {
		if (buffer.size() == 0) return false;
		
		if (buffer.get(0) != HEAD) cleanBuffer();
		
		boolean hasHead = false, hasFoot = false;
		for (Byte b : buffer) {
			if (b == HEAD)
				hasHead = true;
			else if (hasHead && b == FOOT) {
				hasFoot = true;
				break;
			}
		}
		
		return hasFoot;
	}
	
	/**
	 * Cleans the buffer if any bad messages exist.
	 */
	private synchronized void cleanBuffer() {
		while (buffer.size() > 0 && buffer.get(0) != HEAD)
			buffer.remove(0);
	}
}
