package com.eece381.androidcode.communication;

import java.util.List;

import android.graphics.Point;

import com.eece381.androidcode.actor.Player;
import com.eece381.androidcode.game.GameBoard;

/**
 * Package to send to the server. Contains a byte stream of player information.
 * @author Milo
 */
public class OutgoingPackage {
	
	/** The bytes we're planning to send to the server **/
	byte bytesToSend[] = null;
	
	public static final int		messageTypePlayer = 0,
								messageTypeInteraction = 3,
								messageTypeInitialize = 2,
								messageTypeQuery = 1,
								messageTypeUpdate = 4,
								coin = 2,
								gem = 4,
								chest = 3,
								doorOpen = 0,
								doorClosed = 1,
								key = 7,
								lockableDoorOpen = 5,
								lockableDoorClosed = 6,
								speed = 9,
								goggles = 8,
								empty = 11;
	
	/**
	 * Creates a new outgoing package.
	 * @param player - The player data we're going to send to the server.
	 */
	public OutgoingPackage(Player player) {
		bytesToSend = convert(player);
	}
	
	/**
	 * Creates a new outgoing package.
	 * @param cell - the cell we just did something to.
	 * @param cellObject - the type of thing we interacted with.
	 */
	public OutgoingPackage(Point cell, GameBoard.CELL_OBJECT cellObject) {
		bytesToSend = convert(cell, cellObject);
	}
	
	public OutgoingPackage(int map, int countdownTime, int gameTime) {
		bytesToSend = new byte[8];
		bytesToSend[0] = (byte)0xff;
		bytesToSend[1] = (byte)messageTypeQuery;
		bytesToSend[2] = (byte)5;
		bytesToSend[3] = (byte)map;
		bytesToSend[4] = (byte)countdownTime;
		bytesToSend[5] = (byte)gameTime;
		bytesToSend[6] = (byte)0;
		bytesToSend[7] = (byte)0xfe;
	}
	
	/**
	 * Creates a new outgoing package.
	 * @param locations - the locations of the objects.
	 * @param type - the type of object.
	 */
	public OutgoingPackage(List<Point> locations, int type) {
		bytesToSend = convert(locations, type);
	}
	
	/** @return the bytes to send **/
	public byte[] getBytes() { return bytesToSend; }
	
	/**
	 * @override
	 * @param other
	 * @return
	 */
	public boolean equals(OutgoingPackage other) {
		if (other == null || other.bytesToSend == null) 
			return false;
		for (int i = 0; i < bytesToSend.length; ++i)
			if (bytesToSend[i] != other.bytesToSend[i])
				return false;
		return true;
	}
	
	/**
	 * Converts player data into a stream of bytes we can send to the server.
	 * @param player - The player data.
	 * @return the byte stream
	 */
	private byte[] convert(Player player) {
		Point	position	= player.getPosition(),
				target		= player.getTargetLocation(),
				activated	= player.getActivatedPosition();
		
		int powerups = 0;
		if (player.hasSpeedBoost()) powerups += 1;
		if (player.getHasXRayVision()) powerups += 2;
		
		byte[] bytes = {	(byte)0xff,
							(byte)messageTypePlayer,
							(byte)9,
							(byte)player.ID,
							(byte)(position.x),
							(byte)(position.y),
							(byte)(activated.x),
							(byte)(activated.y),
							(byte)(target.x),
							(byte)(target.y),
							(byte)(powerups),
							(byte)(player.getScore())
						};
		
		return bytes;
	}
	
	/**
	 * Converts cell data into a stream of bytes we can send to the server.
	 * @param cell - The cell data.
	 * @return the byte stream
	 */
	private byte[] convert(Point cell, GameBoard.CELL_OBJECT cellObject, boolean interaction) {
		int objectType = 0;
		if (cellObject == GameBoard.CELL_OBJECT.TREASURE_COIN)
			objectType = coin;
		else if (cellObject == GameBoard.CELL_OBJECT.TREASURE_JEWEL)
			objectType = gem;
		else if (cellObject == GameBoard.CELL_OBJECT.TREASURE_CHEST)
			objectType = chest;
		else if (cellObject == GameBoard.CELL_OBJECT.DOOR_OPEN)
			objectType = doorOpen;
		else if (cellObject == GameBoard.CELL_OBJECT.DOOR_CLOSED)
			objectType = doorClosed;
		else if (cellObject == GameBoard.CELL_OBJECT.KEY)
			objectType = key;
		else if (cellObject == GameBoard.CELL_OBJECT.LOCKED_DOOR_CLOSED)
			objectType = lockableDoorClosed;
		else if (cellObject == GameBoard.CELL_OBJECT.LOCKED_DOOR_OPEN)
			objectType = lockableDoorOpen;
		else if (cellObject == GameBoard.CELL_OBJECT.SPEED_BOOST)
			objectType = speed;
		else if (cellObject == GameBoard.CELL_OBJECT.XRAY_VISION)
			objectType = goggles;
			
		byte[] bytes = {	(byte)0xff,
							(byte)messageTypeInteraction,
							(byte)3,
							(byte)cell.x,
							(byte)cell.y,
							(byte)objectType
						};
		return bytes;
	}
	
	/**
	 * Converts cell data into a stream of bytes we can send to the server.
	 * @param cell - The cell data.
	 * @return the byte stream
	 */
	private byte[] convert(Point cell, GameBoard.CELL_OBJECT cellObject) {
		int replaceWith = 0;
		if (cellObject == GameBoard.CELL_OBJECT.TREASURE_COIN)
			replaceWith = empty;
		else if (cellObject == GameBoard.CELL_OBJECT.TREASURE_JEWEL)
			replaceWith = empty;
		else if (cellObject == GameBoard.CELL_OBJECT.TREASURE_CHEST)
			replaceWith = empty;
		else if (cellObject == GameBoard.CELL_OBJECT.DOOR_OPEN)
			replaceWith = doorClosed;
		else if (cellObject == GameBoard.CELL_OBJECT.DOOR_CLOSED)
			replaceWith = doorOpen;
		else if (cellObject == GameBoard.CELL_OBJECT.KEY)
			replaceWith = empty;
		else if (cellObject == GameBoard.CELL_OBJECT.LOCKED_DOOR_CLOSED)
			replaceWith = lockableDoorOpen;
		else if (cellObject == GameBoard.CELL_OBJECT.LOCKED_DOOR_OPEN)
			replaceWith = lockableDoorClosed;
		else if (cellObject == GameBoard.CELL_OBJECT.SPEED_BOOST)
			replaceWith = empty;
		else if (cellObject == GameBoard.CELL_OBJECT.XRAY_VISION)
			replaceWith = empty;
			
		byte[] bytes = {	(byte)0xff,
							(byte)messageTypeUpdate,
							(byte)3,
							(byte)cell.x,
							(byte)cell.y,
							(byte)replaceWith
						};
		return bytes;
	}
	
	/**
	 * Converts object data into a byte stream for the server.
	 * @param locations - the locations of the objects.
	 * @param type - the type of object.
	 */
	public byte[] convert(List<Point> locations, int type) {
		byte[] bytes = new byte[4+2*locations.size()];
		bytes[0] = (byte)0xff;
		bytes[1] = messageTypeInitialize;
		bytes[2] = (byte)(1+2*locations.size());
		bytes[3] = (byte)type;
		for (int i = 0; i < locations.size(); i++) {
			bytes[4+2*i] = (byte)(locations.get(i).x);
			bytes[5+2*i] = (byte)(locations.get(i).y);
		}
		return bytes;
	}
}
