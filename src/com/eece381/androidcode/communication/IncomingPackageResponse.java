package com.eece381.androidcode.communication;

public class IncomingPackageResponse {
	
	private int map = 0;
	private int waitTime = 0;
	private int gameTime = 0;
	private int id = 0;
	
	public IncomingPackageResponse(byte[] bytes) {
		map = bytes[0];
		waitTime = bytes[1];
		gameTime = bytes[2];
		id = bytes[3];
	}
	
	public int getMap() { return map; }
	public int getWaitTimeRemaining() { return waitTime; }
	public int getGameTimeRemaining() { return gameTime; }
	public int getId() { return id; }
}
