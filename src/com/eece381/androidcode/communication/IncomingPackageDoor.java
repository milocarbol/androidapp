package com.eece381.androidcode.communication;

import java.util.LinkedList;
import java.util.List;

import android.graphics.Point;

/**
 * Package received from the server. Contains door information.
 * @author Milo
 */
public class IncomingPackageDoor {

	/** Closed door locations **/
	private List<Point> closedDoors = new LinkedList<Point>();
	
	/** Open door locations **/
	private List<Point> openDoors = new LinkedList<Point>();
	
	/** The number of bytes per door **/
	private static final int numberOfBytesPerDoor = 3;
	
	/** The code for a closed door **/
	private static final int closedCode = 0;
	
	/** The code for a open door **/
	private static final int openCode = 1;
	
	/**
	 * Creates a new IncomingMessageDoor and populates it from bytes.
	 * @param bytes - the bytes.
	 */
	public IncomingPackageDoor(byte[] bytes) {
		for (int i = 0; i <= bytes.length - numberOfBytesPerDoor; i += numberOfBytesPerDoor)
			if (bytes[i+numberOfBytesPerDoor-1] == closedCode)
				closedDoors.add(new Point(bytes[i], bytes[i+1]));
			else if (bytes[i+numberOfBytesPerDoor-1] == openCode)
				openDoors.add(new Point(bytes[i], bytes[i+1]));
	}
	
	/** @return the list of closed door locations. **/
	public List<Point> getClosedDoorLocations() { return closedDoors; }
	
	/** @return the list of open door locations. **/
	public List<Point> getOpenDoorLocations() { return openDoors; }
}
