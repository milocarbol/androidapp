package com.eece381.androidcode.communication;

public class IncomingPackageTimer {

	private int timeRemaining;
	
	public IncomingPackageTimer(byte[] bytes) {
		timeRemaining = bytes[0];
	}
	
	public int getTimeRemaining() { return timeRemaining; }
}
