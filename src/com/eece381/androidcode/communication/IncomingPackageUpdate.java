package com.eece381.androidcode.communication;

import android.graphics.Point;

import com.eece381.androidcode.game.GameBoard;

public class IncomingPackageUpdate {

	private Point location;
	private GameBoard.CELL_OBJECT cellObject;
	
	public IncomingPackageUpdate(byte[] bytes) {
		int code = bytes[2];
		location = new Point(bytes[0], bytes[1]);
		if (code == OutgoingPackage.doorClosed)
			cellObject = GameBoard.CELL_OBJECT.DOOR_CLOSED;
		else if (code == OutgoingPackage.doorOpen)
			cellObject = GameBoard.CELL_OBJECT.DOOR_OPEN;
		else if (code == OutgoingPackage.lockableDoorClosed)
			cellObject = GameBoard.CELL_OBJECT.LOCKED_DOOR_CLOSED;
		else if (code == OutgoingPackage.lockableDoorOpen)
			cellObject = GameBoard.CELL_OBJECT.LOCKED_DOOR_OPEN;
		else
			cellObject = null;
	}
	
	public Point getLocation() { return location; }
	public GameBoard.CELL_OBJECT getCellObject() { return cellObject; }
}
