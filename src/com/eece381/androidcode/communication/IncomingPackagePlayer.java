package com.eece381.androidcode.communication;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import android.graphics.Point;
import android.util.Log;

import com.eece381.androidcode.game.GameBoard;

/**
 * Package received from the server. Contains player information.
 * @author Milo
 */
public class IncomingPackagePlayer {
	
	/** The map of player IDs to current locations **/
	private Map<Integer, Point> playerLocations;
	
	/** The map of player IDs to activated locations **/
	private Map<Integer, Point> playerTargets;
	
	/** The map of player IDs to activated locations **/
	private Map<Integer, Point> playerActivations;
	
	private Map<Integer, Integer> playerPowerups;
	
	private Map<Integer, Integer> playerScores;
	
	/** Game board data **/
	private GameBoard.CELL_OBJECT[][] gameBoard;
	
	/** If the server detected a conflict and is instructing the local player to correct something **/
	private boolean forceUpdate = false;
	
	/** Number of bytes in a package per player **/
	private static final int numberOfBytesPerPlayer = 9;
	
	private byte[] incoming;
	
	/**
	 * Creates a new incoming package object and populates it with player information.
	 * @param incoming - the incoming bytes.
	 */
	public IncomingPackagePlayer(byte[] incoming) {
		Log.d("communicate", "yo: " + incoming.length);
		playerLocations = new HashMap<Integer, Point>();
		playerActivations = new HashMap<Integer, Point>();
		playerTargets = new HashMap<Integer, Point>();
		playerPowerups = new HashMap<Integer, Integer>();
		playerScores = new HashMap<Integer, Integer>();
		this.incoming = incoming;

		for (int i = 0; i <= incoming.length - numberOfBytesPerPlayer; i += numberOfBytesPerPlayer) {
			int playerId = incoming[i];
			playerLocations.put(playerId, new Point(incoming[i+1], incoming[i+2]));
			playerActivations.put(playerId, new Point(incoming[i+3], incoming[i+4]));
			playerTargets.put(playerId, new Point(incoming[i+5], incoming[i+6]));
			playerPowerups.put(playerId, Integer.valueOf(incoming[i+7]));
			playerScores.put(playerId, Integer.valueOf(incoming[i+8]));
		}
		
		Log.d("msgDebug", toString());
	}
	
	public String toString()
	{
		String s = "";
		for (int player : playerLocations.keySet()) {
			s += "Player: " + player + "\n";
			s += "   Location: [" + playerLocations.get(player).x + "," + playerLocations.get(player).y + "]\n";
			s += " Activation: [" + playerActivations.get(player).x + "," + playerActivations.get(player).y + "]\n";
			s += "     Target: [" + playerTargets.get(player).x + "," + playerTargets.get(player).y + "]\n";
		}
		return s;
	}
	
	/**
	 * Gets the location for a given player.
	 * @param playerId - The player's ID.
	 * @return the player's location.
	 */
	public Point getPlayerLocation(int playerId) {
		Point point = playerLocations.get(playerId);
		if (point != null) {
			return new Point(point);
		} 
		throw new RuntimeException(playerId + ": " + printBytes() +"\n" + toString());
	}

	/**
	 * Gets the location a given player is activating.
	 * @param playerId - The player's ID.
	 * @return the location the player is activating.
	 */
	public Point getPlayerActivated(int playerId) {
		return new Point(playerActivations.get(playerId));
	}
	
	/**
	 * Gets the score for a given player.
	 * @param playerId - The player's ID.
	 * @return the player's score.
	 */
	public int getPlayerScore(int playerId) {
		return playerScores.get(playerId);
	}
	
	/**
	 * Gets the target a give player is moving towards.
	 * @param playerId - The player's ID.
	 * @return the location towards which the player is moving.
	 */
	public Point getPlayerTarget(int playerId) {
		return new Point(playerTargets.get(playerId));
	}
	
	/**
	 * Gets the powerups a given player has.
	 * @param playerId - The player's ID.
	 * @return the powerups.
	 */
	public int getPlayerPowerups(int playerId) {
		return playerPowerups.get(playerId);
	}

	/**
	 * Checks what object is in a given cell.
	 * @param column - The column to check.
	 * @param row - The row to check.
	 * @return the object type that is in the cell, or null if it is empty.
	 */
	public GameBoard.CELL_OBJECT objectAt(int column, int row) {
		return gameBoard[column][row];
	}
	
	/** @return if the server is forcing the player to update their data **/
	public boolean serverIsOverriding() { return forceUpdate; }
	
	/** @return the number of players in this package **/
	public int numberOfPlayers() { return playerLocations.size(); }
	
	/** @return the player IDs **/
	public Set<Integer> players() { return playerLocations.keySet(); }
	
	public String printBytes() {
		String s = "";
		for (int i = 0; i < incoming.length; i++)
			s += incoming[i] + " ";
		return s;
	}
}
