package com.eece381.androidcode.game;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.TimerTask;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.eece381.androidcode.EndGameActivity;
import com.eece381.androidcode.MainActivity;
import com.eece381.androidcode.actor.LinearPathfinder;
import com.eece381.androidcode.actor.Player;
import com.eece381.androidcode.actor.PlayerMover;
import com.eece381.androidcode.actor.ServerControlledPlayerManager;
import com.eece381.androidcode.communication.IncomingPackagePlayer;
import com.eece381.androidcode.communication.IncomingPackageTimer;
import com.eece381.androidcode.communication.ServerCommunicator;
import com.eece381.androidcode.communication.TCPReader;
import com.eece381.androidcode.gui.GameView;
import com.eece381.androidcode.gui.RayCaster;

/**
 * Manages all game data, including players, AI, monsters, moveable objects..
 * @author Milo
 */
public class GameManager extends TimerTask {

	/** The manager for players **/
	private ServerControlledPlayerManager playerManager;
	
	/** The mover for players **/
	private PlayerMover playerMover;
	
	/** The game board **/
	private ServerControlledGameBoard gameBoard;
	
	/** The ray caster for visibility **/
	private RayCaster rayCaster;
	
	/** The game view being rendered **/
	private GameView gameView;
	
	/** The communicator. **/
	private ServerCommunicator communicator;
	
	private GameView.Images images;
	
	private Context callingActivity;
	
	private int timeRemaining;
	
	/**
	 * Creates a new ActorManager object.
	 * @param playerManager - The manager for players.
	 * @param gameBoard - The game board.
	 * @param rayCaster - The ray caster for visibility shadows.
	 * @param gameView - The game view.
	 * @param communicator - The communicator.
	 */
	public GameManager(ServerControlledPlayerManager playerManager, PlayerMover playerMover, ServerControlledGameBoard gameBoard, RayCaster rayCaster, GameView gameView, ServerCommunicator communicator, GameView.Images images, int gameTimeRemaining, Context callingActivity) {
		this.playerManager = playerManager;
		this.playerMover = playerMover;
		this.gameBoard = gameBoard;
		this.rayCaster = rayCaster;
		this.gameView = gameView;
		this.communicator = communicator;
		this.images = images;
		this.timeRemaining = gameTimeRemaining;
		this.callingActivity = callingActivity;
	}

	/**
	 * Manages all game data including players, treasure, etc.
	 */
	@Override
	public void run() {
		synchronized(GameView.lock) {
			if(MainActivity.runAllTasks){
				IncomingPackagePlayer incomingPlayer = communicator.getLastPlayer();
				addNewPlayers(incomingPlayer);
				playerManager.updatePlayers(incomingPlayer);
				playerMover.movePlayers();
				gameBoard.updateBoard();
				rayCaster.runRayCaster();
				IncomingPackageTimer incomingTimer = communicator.getTimer();
				if (incomingTimer != null){
					timeRemaining = incomingTimer.getTimeRemaining();
					if(timeRemaining == 0){
						//End game
						endGame();
					}
				}
				gameView.postInvalidate();
			}
		}
	}
	
	private void endGame() {
//		MainActivity.timer.cancel();
		this.cancel();
		MainActivity.timer.purge();
//		TCPReader.getTCPReader(null, null).cancel();
		MainActivity.runAllTasks = false;
		Intent intent = new Intent(callingActivity, EndGameActivity.class);
		String[] resultingPositions = {"firstPlace", "secondPlace", "thirdPlace", "fourthPlace"};
		List<Player> resultingPlayers = playerManager.getPlayers();
		List<Integer> resultingScores = new LinkedList<Integer>();
		for(Player p : resultingPlayers){
			resultingScores.add(p.getScore());
			if(p.isLocal())
				intent.putExtra("localPlayersScore", p.getScore());
		}
		Collections.sort(resultingScores,Collections.reverseOrder());
		
		
		for(int i=0; i < resultingPlayers.size(); i++){
			intent.putExtra(resultingPositions[i],resultingScores.get(i));
		}
		
		intent.putExtra("numPlayers", resultingPlayers.size());
		
		callingActivity.startActivity(intent);
	}
	
	private void addNewPlayers(IncomingPackagePlayer incoming) {
		if (incoming != null) {
			if (playerManager.numberOfPlayers() != playerMover.numberOfPlayers())
				throw new RuntimeException("Player manager and player mover are out of sync: " + playerManager.numberOfPlayers() + " and " + playerMover + ".");
			
			int numberOfPlayers = playerManager.numberOfPlayers();
			
			if (incoming.numberOfPlayers() > numberOfPlayers) {
				Log.d("player", "adding?");
				for (Integer id : incoming.players()) {
					Log.d("player", id + "");
					Player newPlayer = new Player(id, incoming.getPlayerLocation(id), false, new LinearPathfinder(), gameBoard, images, callingActivity);
					playerManager.addPlayer(newPlayer);
					playerMover.addPlayer(newPlayer);
				}
			}
		}
	}
	
	public List<Player> getPlayers() {
		return playerManager.getPlayers();
	}
	
	public String timeRemainingString() {
		return "Time remaining: " + timeRemaining;
	}
}
