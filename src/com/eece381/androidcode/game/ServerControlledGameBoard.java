package com.eece381.androidcode.game;

import java.util.LinkedList;
import java.util.List;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Point;

import com.eece381.androidcode.actor.Player;
import com.eece381.androidcode.communication.IncomingPackageDoor;
import com.eece381.androidcode.communication.IncomingPackageOther;
import com.eece381.androidcode.communication.IncomingPackageTreasure;
import com.eece381.androidcode.communication.IncomingPackageUpdate;
import com.eece381.androidcode.communication.OutgoingPackage;
import com.eece381.androidcode.communication.ServerCommunicator;
import com.eece381.androidcode.gui.GameView;

/**
 * Manages the game board based on server instructions.
 * @author Milo
 */
public class ServerControlledGameBoard extends GameBoard {
	
	private final ServerCommunicator communicator;
	
	/**
	 * Creates a new game board.
	 * @param width - The width in cells of the board.
	 * @param height - The height in cells of the board.
	 * @param communicator - The server communicator.
	 */
	public ServerControlledGameBoard(CELL_OBJECT[][] gameBoard, GameView.Images images, Context callingActivity, ServerCommunicator communicator) {
		super(gameBoard, images, callingActivity);
		this.communicator = communicator;
	}
	
	public void sendToServer() {
		List<Point> coins = new LinkedList<Point>(),
					gems = new LinkedList<Point>(),
					chests = new LinkedList<Point>(),
					doorsOpen = new LinkedList<Point>(),
					doorsClosed = new LinkedList<Point>(),
					goggles = new LinkedList<Point>(),
					speeds = new LinkedList<Point>(),
					keys = new LinkedList<Point>(),
					lockableDoorsOpen = new LinkedList<Point>(),
					lockableDoorsClosed = new LinkedList<Point>();
		for (int column = 0; column < gameBoard.length; column++)
			for (int row = 0; row < gameBoard[0].length; row++) {
				CELL_OBJECT cellObject = gameBoard[column][row];
				Point cell = new Point(column, row);
				if (cellObject == CELL_OBJECT.TREASURE_COIN)
					coins.add(cell);
				else if (cellObject == CELL_OBJECT.TREASURE_JEWEL)
					gems.add(cell);
				else if (cellObject == CELL_OBJECT.TREASURE_CHEST)
					chests.add(cell);
				else if (cellObject == CELL_OBJECT.DOOR_OPEN)
					doorsOpen.add(cell);
				else if (cellObject == CELL_OBJECT.DOOR_CLOSED)
					doorsClosed.add(cell);
				else if (cellObject == CELL_OBJECT.XRAY_VISION)
					goggles.add(cell);
				else if (cellObject == CELL_OBJECT.SPEED_BOOST)
					speeds.add(cell);
				else if (cellObject == CELL_OBJECT.KEY)
					keys.add(cell);
				else if (cellObject == CELL_OBJECT.LOCKED_DOOR_CLOSED)
					lockableDoorsClosed.add(cell);
				else if (cellObject == CELL_OBJECT.LOCKED_DOOR_OPEN)
					lockableDoorsOpen.add(cell);
			}
		
		int shortWaitTime = 1000;
		int longWaitTime = 5000;
		wait(shortWaitTime);
		send(gems, OutgoingPackage.gem);
		wait(shortWaitTime);
		send(chests, OutgoingPackage.chest);
		wait(shortWaitTime);
		send(goggles, OutgoingPackage.goggles);
		wait(shortWaitTime);
		send(speeds, OutgoingPackage.speed);
		wait(shortWaitTime);
		send(keys, OutgoingPackage.key);
		wait(shortWaitTime);
		send(lockableDoorsClosed, OutgoingPackage.lockableDoorClosed);
		wait(shortWaitTime);
		send(lockableDoorsOpen, OutgoingPackage.lockableDoorOpen);
		wait(shortWaitTime);
		send(doorsOpen, OutgoingPackage.doorOpen);
		wait(longWaitTime);
		send(doorsClosed, OutgoingPackage.doorClosed);
		wait(longWaitTime);
		send(coins, OutgoingPackage.coin);
		wait(longWaitTime+shortWaitTime);
	}
	
	public static void wait(int millis) {
		int startTime = (int)System.currentTimeMillis();
		while ((int)System.currentTimeMillis() - startTime < millis);
	}
	
	/**
	 * Sends to the server.
	 * @param locations - The locations.
	 * @param type - the types of objects.
	 */
	private void send(List<Point> locations, int type) {
		communicator.send(new OutgoingPackage(locations, type));
	}
	
	/**
	 * Sends to the server.
	 * @param cell - the location of the object.
	 * @param type - the object.
	 */
	private void send(Point cell, CELL_OBJECT type) {
		communicator.send(new OutgoingPackage(cell, type));
	}
	
	@Override
	public void updateBoard() {
//		IncomingPackageTreasure coins = communicator.getLastCoin();
//		IncomingPackageTreasure gems = communicator.getLastGem();
//		IncomingPackageTreasure chests = communicator.getLastChest();
//		IncomingPackageDoor doors = communicator.getLastDoor();
//		IncomingPackageOther others = communicator.getLastOther();
//		updateBoard(coins, OutgoingPackage.coin);
//		updateBoard(gems, OutgoingPackage.gem);
//		updateBoard(chests, OutgoingPackage.chest);
//		updateBoard(doors);
//		updateBoard(others);
		IncomingPackageUpdate update = communicator.getUpdate();
		
		if (update == null) return;
		
		Point location = update.getLocation();
		CELL_OBJECT cellObject = update.getCellObject();
		gameBoard[location.x][location.y] = cellObject;
		if (cellObject == CELL_OBJECT.DOOR_CLOSED)
			imageBoard.closeDoor(location);
		else if (cellObject == CELL_OBJECT.DOOR_OPEN)
			imageBoard.openDoor(location);
		else if (cellObject == CELL_OBJECT.LOCKED_DOOR_CLOSED)
			imageBoard.closeLockedDoor(location);
		else if (cellObject == CELL_OBJECT.LOCKED_DOOR_OPEN)
			imageBoard.openLockedDoor(location);
		else
			imageBoard.clear(location);
	}
	
	/**
	 * Updates treasure of a given type.
	 * @param incoming - the incoming treasure package.
	 * @param type - the type of treasure.
	 */
	public void updateBoard(IncomingPackageTreasure incoming, int type) {
		if (incoming == null) return;
		
		CELL_OBJECT cellObject;
		Bitmap image;
		
		if (type == OutgoingPackage.coin) {
			cellObject = CELL_OBJECT.TREASURE_COIN;
			image = images.treasureCoin;
		}
		else if (type == OutgoingPackage.gem) {
			cellObject = CELL_OBJECT.TREASURE_JEWEL;
			image = images.treasureJewel;
		}
		else {
			cellObject = CELL_OBJECT.TREASURE_CHEST;
			image = images.treasureChest;
		}
		
		updateInstancesOfObject(cellObject, incoming.getTreasureLocations(), image);
	}
	
	/**
	 * Updates doors.
	 * @param incoming - the incoming door package.
	 */
	public void updateBoard(IncomingPackageDoor incoming) {
		if (incoming == null) return;
		
		for (Point location : incoming.getClosedDoorLocations()) {
			gameBoard[location.x][location.y] = CELL_OBJECT.DOOR_CLOSED;
			imageBoard.closeDoor(location);
		}
		for (Point location : incoming.getOpenDoorLocations()) {
			gameBoard[location.x][location.y] = CELL_OBJECT.DOOR_OPEN;
			imageBoard.openDoor(location);
		}
	}
	
	/**
	 * Updates other stuff.
	 * @param incoming - The incoming package.
	 */
	public void updateBoard(IncomingPackageOther incoming) {
		if (incoming == null) return;
		
		updateInstancesOfObject(CELL_OBJECT.KEY, incoming.getKeyLocations(), images.key);
		updateInstancesOfObject(CELL_OBJECT.SPEED_BOOST, incoming.getSpeedBoostLocations(), images.speedBoost);
		updateInstancesOfObject(CELL_OBJECT.XRAY_VISION, incoming.getGoggleLocations(), images.xrayVision);
		
		for (Point location : incoming.getClosedLockableDoorLocations()) {
			gameBoard[location.x][location.y] = CELL_OBJECT.LOCKED_DOOR_CLOSED;
			imageBoard.closeLockedDoor(location);
		}
		for (Point location : incoming.getOpenLockableDoorLocations()) {
			gameBoard[location.x][location.y] = CELL_OBJECT.LOCKED_DOOR_OPEN;
			imageBoard.openLockedDoor(location);
		}
	}
	
	/**
	 * Removes consumables that are no longer there and adds newly found consumables.
	 * @param cellObject
	 * @param locations
	 */
	private void updateInstancesOfObject(CELL_OBJECT cellObject, List<Point> locations, Bitmap image) {
		for (int column = 0; column < gameBoard.length; column++)
			for (int row = 0; row < gameBoard[0].length; row++)
				if (gameBoard[column][row] == cellObject) {
					gameBoard[column][row] = null;
					imageBoard.clear(new Point(column, row));
				}
		for (Point location : locations) {
			gameBoard[location.x][location.y] = cellObject;
			imageBoard.addImage(location, image);
		}
	}
	
	@Override
	public void interactWithCell(Point pos, Player player) {
		CELL_OBJECT cellObject = gameBoard[pos.x][pos.y];
		if (player.getHasKey() ||
				(cellObject != CELL_OBJECT.LOCKED_DOOR_CLOSED &&
				cellObject != CELL_OBJECT.LOCKED_DOOR_OPEN))
			send(pos, cellObject);
		super.interactWithCell(pos, player);
	}
}
