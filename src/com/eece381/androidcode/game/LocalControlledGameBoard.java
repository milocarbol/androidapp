package com.eece381.androidcode.game;

import android.content.Context;

import com.eece381.androidcode.gui.GameView;

public class LocalControlledGameBoard extends GameBoard {
	
	public LocalControlledGameBoard(CELL_OBJECT[][] gameBoard, GameView.Images images, Context callingActivity) {
		super(gameBoard, images, callingActivity);

//		gameBoard[1][2] = CELL_OBJECT.WALL;
//		gameBoard[2][2] = CELL_OBJECT.WALL;
//		
//		gameBoard[2][1] = CELL_OBJECT.DOOR_CLOSED;
//		
//		gameBoard[2][3] = CELL_OBJECT.WALL;
//		gameBoard[2][4] = CELL_OBJECT.WALL;
//		gameBoard[2][5] = CELL_OBJECT.WALL;
//		gameBoard[2][6] = CELL_OBJECT.WALL;
//		gameBoard[2][7] = CELL_OBJECT.WALL;
//		
//		gameBoard[2][0] = CELL_OBJECT.WALL;
//		gameBoard[3][0] = CELL_OBJECT.WALL;
//		gameBoard[4][0] = CELL_OBJECT.WALL;
//		gameBoard[5][0] = CELL_OBJECT.WALL;
//		gameBoard[6][0] = CELL_OBJECT.WALL;
//		gameBoard[7][0] = CELL_OBJECT.WALL;
//		gameBoard[8][0] = CELL_OBJECT.WALL;
//		
//		gameBoard[8][1] = CELL_OBJECT.WALL;
//		gameBoard[8][2] = CELL_OBJECT.DOOR_CLOSED;
//		gameBoard[8][3] = CELL_OBJECT.WALL;
//		gameBoard[8][4] = CELL_OBJECT.WALL;
//		gameBoard[8][5] = CELL_OBJECT.DOOR_CLOSED;
//		gameBoard[8][6] = CELL_OBJECT.WALL;
//		gameBoard[8][7] = CELL_OBJECT.WALL;
//		
//		gameBoard[3][7] = CELL_OBJECT.WALL;
//		gameBoard[4][7] = CELL_OBJECT.WALL;
//		gameBoard[5][7] = CELL_OBJECT.WALL;
//		gameBoard[6][7] = CELL_OBJECT.DOOR_CLOSED;
//		gameBoard[7][7] = CELL_OBJECT.WALL;
//		
//		//SECOND ROOM
//		gameBoard[9][3] = CELL_OBJECT.WALL;
//		gameBoard[10][3] = CELL_OBJECT.WALL;
//		gameBoard[11][3] = CELL_OBJECT.WALL;
//		gameBoard[12][3] = CELL_OBJECT.WALL;
//		gameBoard[13][3] = CELL_OBJECT.WALL;
//		
//		gameBoard[13][4] = CELL_OBJECT.WALL;
//		gameBoard[13][5] = CELL_OBJECT.WALL;
//		gameBoard[13][6] = CELL_OBJECT.WALL;
//		gameBoard[13][7] = CELL_OBJECT.WALL;
//		
//		gameBoard[9][7] = CELL_OBJECT.WALL;
//		gameBoard[10][7] = CELL_OBJECT.WALL;
//		gameBoard[11][7] = CELL_OBJECT.WALL;
//		gameBoard[12][7] = CELL_OBJECT.WALL;
//		gameBoard[13][7] = CELL_OBJECT.WALL;
//		
//		gameBoard[12][4] = CELL_OBJECT.TREASURE;
//		gameBoard[12][5] = CELL_OBJECT.TREASURE;
//		gameBoard[12][6] = CELL_OBJECT.TREASURE;
//		
//		//THIRD ROOM
//		gameBoard[2][8] = CELL_OBJECT.WALL;
//		gameBoard[2][9] = CELL_OBJECT.WALL;
//		gameBoard[2][10] = CELL_OBJECT.WALL;
//		gameBoard[2][11] = CELL_OBJECT.WALL;
//		gameBoard[2][12] = CELL_OBJECT.WALL;
//		gameBoard[2][13] = CELL_OBJECT.WALL;
//		gameBoard[2][14] = CELL_OBJECT.WALL;
//
//		for(int i = 0; i < 20; i++){
//			gameBoard[i][14] = CELL_OBJECT.WALL;
//			gameBoard[i][19] = CELL_OBJECT.WALL;
//			gameBoard[19][i] = CELL_OBJECT.WALL;
//		}
//		gameBoard[1][14] = CELL_OBJECT.DOOR_CLOSED;
//		gameBoard[18][14] = CELL_OBJECT.DOOR_CLOSED;
//		
//		gameBoard[8][8] = CELL_OBJECT.WALL;
//		gameBoard[8][9] = CELL_OBJECT.WALL;
//		gameBoard[8][10] = CELL_OBJECT.WALL;
//		gameBoard[8][11] = CELL_OBJECT.WALL;
//		gameBoard[8][12] = CELL_OBJECT.DOOR_CLOSED;
//		gameBoard[8][13] = CELL_OBJECT.WALL;
//		
//		//FOURTH ROOM
//		gameBoard[9][1] = CELL_OBJECT.WALL;
//		gameBoard[10][1] = CELL_OBJECT.WALL;
//		gameBoard[11][1] = CELL_OBJECT.WALL;
//		gameBoard[11][0] = CELL_OBJECT.WALL;
//		gameBoard[12][0] = CELL_OBJECT.WALL;
//		gameBoard[13][0] = CELL_OBJECT.WALL;
//		gameBoard[14][0] = CELL_OBJECT.WALL;
//		gameBoard[15][0] = CELL_OBJECT.WALL;
//		gameBoard[16][0] = CELL_OBJECT.WALL;
//		gameBoard[17][0] = CELL_OBJECT.WALL;
//		
//
//		gameBoard[17][1] = CELL_OBJECT.WALL;
//		gameBoard[17][2] = CELL_OBJECT.WALL;
//		gameBoard[17][3] = CELL_OBJECT.WALL;
//		gameBoard[17][4] = CELL_OBJECT.DOOR_CLOSED;
//		gameBoard[17][5] = CELL_OBJECT.WALL;
//		gameBoard[17][6] = CELL_OBJECT.WALL;
//		gameBoard[17][7] = CELL_OBJECT.WALL;
//		gameBoard[17][8] = CELL_OBJECT.WALL;
//		gameBoard[17][9] = CELL_OBJECT.WALL;
//		gameBoard[17][10] = CELL_OBJECT.WALL;
//		
//		gameBoard[16][5] = CELL_OBJECT.WALL;
//		gameBoard[15][5] = CELL_OBJECT.WALL;
//		gameBoard[15][4] = CELL_OBJECT.WALL;
//		gameBoard[15][3] = CELL_OBJECT.WALL;
//		gameBoard[15][2] = CELL_OBJECT.WALL;
//		gameBoard[14][2] = CELL_OBJECT.WALL;
//		gameBoard[13][2] = CELL_OBJECT.WALL;
//		
//		gameBoard[18][0] = CELL_OBJECT.WALL;
//		gameBoard[18][1] = CELL_OBJECT.TREASURE;
//		
//		//isle of doors
//		gameBoard[18][5] = CELL_OBJECT.DOOR_CLOSED;
//		gameBoard[18][6] = CELL_OBJECT.DOOR_CLOSED;
//		gameBoard[18][7] = CELL_OBJECT.DOOR_CLOSED;
//		gameBoard[18][8] = CELL_OBJECT.DOOR_CLOSED;
//		gameBoard[18][9] = CELL_OBJECT.DOOR_CLOSED;
//		gameBoard[18][10] = CELL_OBJECT.DOOR_CLOSED;
//		
//		gameBoard[14][7] = CELL_OBJECT.WALL;
//		gameBoard[15][7] = CELL_OBJECT.WALL;
//		gameBoard[16][7] = CELL_OBJECT.DOOR_CLOSED;
//		
//
//		gameBoard[14][3] = CELL_OBJECT.TREASURE;
//		
//		gameBoard[9][11] = CELL_OBJECT.WALL;
//		gameBoard[10][11] = CELL_OBJECT.WALL;
//		gameBoard[10][9] = CELL_OBJECT.WALL;
//		gameBoard[10][10] = CELL_OBJECT.WALL;
//		gameBoard[10][12] = CELL_OBJECT.WALL;
//		gameBoard[11][9] = CELL_OBJECT.WALL;
//		gameBoard[11][10] = CELL_OBJECT.WALL;
//		gameBoard[11][11] = CELL_OBJECT.TREASURE;
//		gameBoard[11][12] = CELL_OBJECT.WALL;
//		
//
//		gameBoard[12][10] = CELL_OBJECT.WALL;
//		gameBoard[13][10] = CELL_OBJECT.WALL;
//		gameBoard[14][10] = CELL_OBJECT.WALL;
//		gameBoard[15][10] = CELL_OBJECT.DOOR_CLOSED;
//		gameBoard[16][10] = CELL_OBJECT.WALL;
//		gameBoard[17][10] = CELL_OBJECT.WALL;
//		
//
//		gameBoard[12][12] = CELL_OBJECT.WALL;
//		gameBoard[13][12] = CELL_OBJECT.WALL;
//		gameBoard[13][13] = CELL_OBJECT.DOOR_CLOSED;
//		
//		gameBoard[13][11] = CELL_OBJECT.DOOR_CLOSED;
//		gameBoard[9][10] = CELL_OBJECT.TREASURE;
//		
//
//		gameBoard[5][3] = CELL_OBJECT.TREASURE;
//		gameBoard[3][13] = CELL_OBJECT.TREASURE;
//		gameBoard[14][1] = CELL_OBJECT.TREASURE;
		
	}
	
	@Override
	public void updateBoard() {
		// TODO Auto-generated method stub

	}
}
