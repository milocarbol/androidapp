package com.eece381.androidcode.game;

import java.io.IOException;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;

import com.eece381.androidcode.R;
import com.eece381.androidcode.game.GameBoard.CELL_OBJECT;

/**
 * Loads a map from an image using valid colors. Valid colors are as follows:
 * Black: (255, 255, 255) -- wall
 * Blue: (0, 0, 255) -- water
 * Cyan: (0, 255, 255) -- bridge
 * Dark grey: (68, 68, 68) -- closed door
 * Grey: (136, 136, 136) -- rock
 * Green: (0, 255, 255) -- tree
 * Light grey: (204, 204, 204) -- open door
 * Magenta: (255, 0, 255) -- XRay vision
 * Red: (255, 0, 0) -- speed boost
 * White: (0, 0, 0) -- empty
 * Yellow: (255, 255, 0) -- treasure coin
 * Dark Teal: (0, 140, 140) -- treasure jewel
 * Darker Yellow: (140, 140, 0) -- treasure chest
 * Dark Red: (100, 0, 0) -- locked closed door
 * Dark Green: (0, 100, 0) -- locked open door
 * Dark Blue: (0, 0, 100) -- key
 * 
 * NOTE: You MUST modify your colors in a drawing program to these values!
 * @author Milo
 */
public class MapLoader {
	
	/** Color to use for treasure Coin. **/
	private static final int treasureCoin = Color.YELLOW;
	
	/** Color to use for treasure Jewel. **/
	private static final int treasureJewel = 0xFF008C8C;
	
	/** Color to use for treasure Coin. **/
	private static final int treasureChest = 0xFF8C8C00;
	
	/** Color to use for treasure. **/
	private static final int doorClosed = Color.DKGRAY;
	
	/** Color to use for treasure. **/
	private static final int doorOpen = Color.LTGRAY;
	
	/** Color to use for treasure. **/
	private static final int wall = Color.BLACK;
	
	/** Color to use for Speed Boost. **/
	private static final int speedBoost = Color.RED;
	
	/** Color to use for XRay Vision. **/
	private static final int xRayVision = Color.MAGENTA;
	
	/** Color to use for trees. **/
	private static final int tree = Color.GREEN;
	
	/** Color to use for rock piles. **/
	private static final int rock = Color.GRAY;
	
	/** Color to use for water. **/
	private static final int water = Color.BLUE;
	
	/** Color to use for bridges. **/
	private static final int bridge = Color.CYAN;
	
	/** Color to use for locked closed doors. **/
	private static final int locked_door_closed = 0xFF640000;
	
	/** Color to use for locked open doors. **/
	private static final int locked_door_open = 0xFF006400;
	
	/** Color to use for keys. **/
	private static final int key = 0xFF000064;
	
	/**
	 * Generates the map based on the desired image file.
	 * @param map - The map number.
	 * @param res - The resources object with the image.
	 * @return the game board map.
	 * @throws IOException if an invalid map is selected, or if the map file is corrupted.
	 */
	public static GameBoard.CELL_OBJECT[][] generateMap(int map, Resources res) throws IOException {
		if (map == 1)
			return process(R.drawable.map, res);
		else if (map == 2)
			return process(R.drawable.map2, res);
		else if (map == 3)
			return process(R.drawable.map3, res);
		else if (map == 4)
			return process(R.drawable.map4, res);
		else
			throw new IOException("Invalid map.");
	}
	
	/**
	 * Processes a given map file into a game board.
	 * @param src - The image file.
	 * @param res - The resources with the image.
	 * @return the game board map.
	 * @throws IOException if the map file is corrupted.
	 */
	private static GameBoard.CELL_OBJECT[][] process(int src, Resources res) throws IOException {
		Bitmap img = BitmapFactory.decodeResource(res, src);
		int width = img.getWidth();
		int height = img.getHeight();
		GameBoard.CELL_OBJECT[][] processed = new GameBoard.CELL_OBJECT[width][height];
		for (int column = 0; column < width; column++)
			for (int row = 0; row < height; row++)
				processed[column][row] = type(img.getPixel(column, row));
		return processed;
	}
	
	/**
	 * Gets the cell object type corresponding to a colour in the image.
	 * @param color - The color of the pixel.
	 * @return the cell object type.
	 */
	private static GameBoard.CELL_OBJECT type(int color) {
		switch (color) {
		case wall:
			return GameBoard.CELL_OBJECT.WALL;
		case doorClosed:
			return GameBoard.CELL_OBJECT.DOOR_CLOSED;
		case doorOpen:
			return GameBoard.CELL_OBJECT.DOOR_OPEN;
		case treasureCoin:
			return GameBoard.CELL_OBJECT.TREASURE_COIN;
		case treasureJewel:
			return GameBoard.CELL_OBJECT.TREASURE_JEWEL;
		case treasureChest:
			return GameBoard.CELL_OBJECT.TREASURE_CHEST;
		case speedBoost:
			return GameBoard.CELL_OBJECT.SPEED_BOOST;
		case xRayVision:
			return GameBoard.CELL_OBJECT.XRAY_VISION;
		case tree:
			return GameBoard.CELL_OBJECT.TREE;
		case rock:
			return GameBoard.CELL_OBJECT.ROCK;
		case water:
			return GameBoard.CELL_OBJECT.WATER;
		case bridge:
			return GameBoard.CELL_OBJECT.BRIDGE;
		case locked_door_closed:
			return GameBoard.CELL_OBJECT.LOCKED_DOOR_CLOSED;
		case locked_door_open:
			return GameBoard.CELL_OBJECT.LOCKED_DOOR_OPEN;
		case key:
			return GameBoard.CELL_OBJECT.KEY;
		}
		return null;
	}
}
