package com.eece381.androidcode.game;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.media.MediaPlayer;

import com.eece381.androidcode.R;
import com.eece381.androidcode.actor.Player;
import com.eece381.androidcode.gui.GameView;
import com.eece381.androidcode.gui.ImageBoard;

/**
 * Gameboard of various types. Could be server-controlled, etc.
 * @author Milo
 */
public abstract class GameBoard {

	public final int width;
	public final int height;
	
	protected CELL_OBJECT gameBoard[][];
	
	private Context callingActivity;
	
	private MediaPlayer soundEffect;

	/** Possible objects in a cell **/
	public static enum CELL_OBJECT {	PLAYER,
										TREASURE_COIN,
										TREASURE_JEWEL,
										TREASURE_CHEST,
										WALL,
										DOOR_OPEN,
										DOOR_CLOSED,
										OBSTACLE,
										SPEED_BOOST,
										XRAY_VISION,
										TREE,
										ROCK,
										WATER,
										BRIDGE,
										KEY,
										LOCKED_DOOR_OPEN,
										LOCKED_DOOR_CLOSED};
										
	/** Cell values which cast a vision blocking shadow **/									
	protected static CELL_OBJECT visionBlockingElements[] = { 	CELL_OBJECT.WALL,
																CELL_OBJECT.OBSTACLE,
																CELL_OBJECT.DOOR_CLOSED,
																CELL_OBJECT.LOCKED_DOOR_CLOSED};
	
	/** Cell values which the player cannot pass through **/
	protected static CELL_OBJECT movementBlockingElements[] = {	CELL_OBJECT.WALL,
																CELL_OBJECT.OBSTACLE,
																CELL_OBJECT.DOOR_CLOSED,
																CELL_OBJECT.PLAYER,
																CELL_OBJECT.TREE,
																CELL_OBJECT.ROCK,
																CELL_OBJECT.WATER,
																CELL_OBJECT.TREASURE_COIN,
																CELL_OBJECT.TREASURE_JEWEL,
																CELL_OBJECT.TREASURE_CHEST,
																CELL_OBJECT.SPEED_BOOST,
																CELL_OBJECT.XRAY_VISION,
																CELL_OBJECT.KEY,
																CELL_OBJECT.LOCKED_DOOR_CLOSED};
	
	/** Cell values which contain universally visible elements **/
	protected static CELL_OBJECT universallyVisibleElements[] = {	CELL_OBJECT.WALL,
																	CELL_OBJECT.DOOR_CLOSED,
																	CELL_OBJECT.DOOR_OPEN,
																	CELL_OBJECT.LOCKED_DOOR_CLOSED,
																	CELL_OBJECT.LOCKED_DOOR_OPEN};
	
	/** Cell values which can be interacted with **/
	protected static CELL_OBJECT interactionElements[] = {	CELL_OBJECT.DOOR_CLOSED, 
															CELL_OBJECT.DOOR_OPEN,
															CELL_OBJECT.TREASURE_COIN,
															CELL_OBJECT.TREASURE_JEWEL,
															CELL_OBJECT.TREASURE_CHEST,
															CELL_OBJECT.SPEED_BOOST,
															CELL_OBJECT.XRAY_VISION,
															CELL_OBJECT.KEY,
															CELL_OBJECT.LOCKED_DOOR_CLOSED,
															CELL_OBJECT.LOCKED_DOOR_OPEN};
	protected ImageBoard imageBoard;
	
	protected GameView.Images images;

	public GameBoard(int width, int height) {
		this.width = width;
		this.height = height;
	}
	
	public GameBoard(CELL_OBJECT[][] gameBoard, GameView.Images images, Context callingActivity) {
		this(gameBoard.length, gameBoard[0].length);
		this.gameBoard = gameBoard;
		this.images = images;
		this.callingActivity = callingActivity;
	}
	
	public void initializeImages() {
		imageBoard = new ImageBoard(this, images);
	}
	
	/**
	 * Updates the board.
	 */
	public abstract void updateBoard();
	
	/**
	 * Get the cell value of the game board
	 */
	public CELL_OBJECT getCellValue(int column, int row)
	{
		return gameBoard[column][row];
	}
	
	public Bitmap imageAt(Point cell) {
		return imageBoard.imageAt(cell);
	}
	
	/**
	 * Checks if a space is free.
	 */
	public boolean spaceIsFree(int column, int row) {
		return gameBoard[column][row] == null;
	}
	
	/**
	 * Checks if a space is free.
	 */
	public boolean spaceIsFree(Point cell) {
		return spaceIsFree(cell.x, cell.y);
	}
	
	/**
	 * Checks if a certain cell is vision blocking
	 */
	public boolean isVisionBlocking(int column, int row) {
		for(int i = 0; i < visionBlockingElements.length; i++)
		{
			if(gameBoard[column][row] == visionBlockingElements[i])
				return true;
		}
		
		return false;
	}
	
	/**
	 * Checks if a certain cell is movement blocking
	 */
	public boolean isMovementBlocking(int column, int row) {
		for(int i = 0; i < movementBlockingElements.length; i++)
		{
			if(gameBoard[column][row] == movementBlockingElements[i])
				return true;
		}
		
		return false;
	}
	
	/**
	 * Checks if a certain cell is movement blocking
	 */
	public boolean isMovementBlocking(Point pos) {
		for(int i = 0; i < movementBlockingElements.length; i++)
		{
			if(gameBoard[pos.x][pos.y] == movementBlockingElements[i])
				return true;
		}
		
		return false;
	}
	
	/**
	 * Checks if a certain cell is universally visible
	 */
	
	public boolean isUniversallyVisible(int column, int row) {
		for(int i = 0; i < universallyVisibleElements.length; i++)
		{
			if(gameBoard[column][row] == universallyVisibleElements[i])
				return true;
		}
		
		return false;
	}
	
	/**
	 * Checks if a certain cell can be interacted with
	 */
	
	public boolean isInteractableElement(Point pos) {
		for(int i = 0; i < interactionElements.length; i++)
		{
			if(gameBoard[pos.x][pos.y] == interactionElements[i])
				return true;
		}
		
		return false;
	}
	
	/**
	 * Interacts with Elements
	 */
	
	public void interactWithCell(Point pos, Player player) {
		if(gameBoard[pos.x][pos.y] == CELL_OBJECT.DOOR_OPEN){
			//Close Door
			gameBoard[pos.x][pos.y] = CELL_OBJECT.DOOR_CLOSED;
			imageBoard.closeDoor(pos);
			soundEffect = MediaPlayer.create(callingActivity, R.raw.door_close);
			soundEffect.start();
		}
		else if(gameBoard[pos.x][pos.y] == CELL_OBJECT.DOOR_CLOSED){
			//Open Door
			gameBoard[pos.x][pos.y] = CELL_OBJECT.DOOR_OPEN;
			imageBoard.openDoor(pos);
			soundEffect = MediaPlayer.create(callingActivity, R.raw.door_open);
			soundEffect.start();
		}
		else if(gameBoard[pos.x][pos.y] == CELL_OBJECT.TREASURE_COIN || 
				gameBoard[pos.x][pos.y] == CELL_OBJECT.TREASURE_JEWEL ||
				gameBoard[pos.x][pos.y] == CELL_OBJECT.TREASURE_CHEST) {
			//Get Treasure
			imageBoard.clear(pos);
			if(gameBoard[pos.x][pos.y] == CELL_OBJECT.TREASURE_COIN){
				player.addScore(1);
				soundEffect = MediaPlayer.create(callingActivity, R.raw.coin);
			}
			if(gameBoard[pos.x][pos.y] == CELL_OBJECT.TREASURE_JEWEL){
				player.addScore(5);
				soundEffect = MediaPlayer.create(callingActivity, R.raw.jewel);
			}
			if(gameBoard[pos.x][pos.y] == CELL_OBJECT.TREASURE_CHEST){
				player.addScore(10);
				soundEffect = MediaPlayer.create(callingActivity, R.raw.chest);
			}
			gameBoard[pos.x][pos.y] = null;
			soundEffect.start();
		}else if(gameBoard[pos.x][pos.y] == CELL_OBJECT.SPEED_BOOST) {
			//Speed up
			player.setSpeed(Player.highSpeed);
			player.setSpeedBoostCounter(400);
			imageBoard.clear(pos);
			gameBoard[pos.x][pos.y] = null;
		}else if(gameBoard[pos.x][pos.y] == CELL_OBJECT.XRAY_VISION) {
			//See through walls
			player.setHasXRayVision(true);
			player.setXRayCounter(400);
			imageBoard.clear(pos);
			gameBoard[pos.x][pos.y] = null;
		}else if(gameBoard[pos.x][pos.y] == CELL_OBJECT.LOCKED_DOOR_OPEN) {
			//Check for key
			if(player.getHasKey()){
				//Close Door
				gameBoard[pos.x][pos.y] = CELL_OBJECT.LOCKED_DOOR_CLOSED;
				imageBoard.closeLockedDoor(pos);
				soundEffect = MediaPlayer.create(callingActivity, R.raw.door_close);
				soundEffect.start();
			}else{
				//Inform the user the player must have the key in order to close this door
			}
		}else if(gameBoard[pos.x][pos.y] == CELL_OBJECT.LOCKED_DOOR_CLOSED) {
			//Check for key
			if(player.getHasKey()){
				//Open Door
				gameBoard[pos.x][pos.y] = CELL_OBJECT.LOCKED_DOOR_OPEN;
				imageBoard.openLockedDoor(pos);
				soundEffect = MediaPlayer.create(callingActivity, R.raw.secret);
				soundEffect.start();
			}else{
				//Inform the user the player must have the key in order to open this door
			}
		}else if(gameBoard[pos.x][pos.y] == CELL_OBJECT.KEY) {
			player.pickUpKey();
			gameBoard[pos.x][pos.y] = null;
			imageBoard.clear(pos);
			soundEffect = MediaPlayer.create(callingActivity, R.raw.cog);
			soundEffect.start();
		}
	}
}
