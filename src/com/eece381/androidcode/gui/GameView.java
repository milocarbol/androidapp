package com.eece381.androidcode.gui;

import java.io.IOException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.RectF;
import android.os.CountDownTimer;
import android.view.MotionEvent;
import android.view.View;

import com.eece381.androidcode.MainActivity;
import com.eece381.androidcode.R;
import com.eece381.androidcode.actor.LinearPathfinder;
import com.eece381.androidcode.actor.Player;
import com.eece381.androidcode.actor.PlayerMover;
import com.eece381.androidcode.actor.ServerControlledPlayerManager;
import com.eece381.androidcode.communication.ServerCommunicator;
import com.eece381.androidcode.communication.TCPReader;
import com.eece381.androidcode.game.GameManager;
import com.eece381.androidcode.game.MapLoader;
import com.eece381.androidcode.game.ServerControlledGameBoard;

public class GameView extends View {
	
	public static Object lock = new Object();
	
	/** Object responsible for calculating the visiblity of the player **/
	private RayCaster rayCaster;
	
	/** Manager for players **/
	private ServerControlledPlayerManager playerManager;
	
	/** Object containing the game board **/
	private ServerControlledGameBoard gameBoard;
	
	/** Mover for players **/
	private PlayerMover playerMover;
	
	/** The game manager **/
	private GameManager gameManager;

	/** Object containing player info **/
	private Player localPlayer;
	
	/** The object communicating with the server **/
	ServerCommunicator serverCommunicator;
	
	/** The server reader **/
	TCPReader tcpReader;
	
	/** Timer Delay for backend code **/
	public static final long gameDelay = 30;
	
	/** The screen controller **/
	private MovingScreen screen;
	
	/** Image data **/
	private final Images images = new Images();
	
	/** The calling activity **/
	public Context callingActivity;
	
	/** Timer counting to start of game **/
	private String startGameTimerString = "";
	private boolean isGameStarted;
	
	/**
	 * Creates the game window.
	 * @param context - The app context.
	 */
	public GameView(Context context, ServerCommunicator serverCommunicator, TCPReader tcpReader, int map, int waitTimeRemaining, int gameTimeRemaining, boolean isHosting, int playerId) {
		super(context);
		callingActivity = context;
		try {
			initialize(serverCommunicator,tcpReader, map, waitTimeRemaining, gameTimeRemaining, isHosting, playerId);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
	
	/**
	 * Initializes all game management systems.
	 * @throws IOException if the map cannot be loaded.
	 */
	private void initialize(ServerCommunicator serverCommunicator, TCPReader tcpReader, int map, int waitTimeRemaining, int gameTimeRemaining, boolean isHosting, int playerId) throws IOException {
		
		//Start countdown timer
		new CountDownTimer(waitTimeRemaining * 1000, 1000) {
		     public void onTick(long millisUntilFinished) {
		    	 startGameTimerString = "" + (millisUntilFinished / 1000);
		     }

		     public void onFinish() {
		    	 startGameTimerString = "GO!";
		    	 isGameStarted = true;
		     }
		  }.start();

		this.serverCommunicator = serverCommunicator;
		this.tcpReader = tcpReader;
		
		gameBoard = new ServerControlledGameBoard(MapLoader.generateMap(map, getResources()), images, callingActivity, serverCommunicator);
		
//		if (isHosting) gameBoard.sendToServer();
		
		localPlayer = new Player(playerId, new Point(1,1), true, new LinearPathfinder(), gameBoard,images, callingActivity);
		playerManager = new ServerControlledPlayerManager(localPlayer, serverCommunicator);
		playerMover = new PlayerMover(localPlayer);
		screen = new MovingScreen(localPlayer, gameBoard.width, gameBoard.height, 6, 3);
		rayCaster = new RayCaster(this, screen, localPlayer, gameBoard);
		
		gameManager = new GameManager(playerManager, playerMover, gameBoard, rayCaster, this, serverCommunicator, images, gameTimeRemaining, callingActivity);
		
		MainActivity.timer.schedule(gameManager, 0, gameDelay);
		
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent tapped)
	{
		if(isGameStarted){
			Point target = screen.pixelToAbsoluteCell(localPlayer.getPosition(), (int)tapped.getX(), (int)tapped.getY(), getWidth(), getHeight());
			playerManager.tap(screen.validate(target));
		}
		return true;
	}
	
	
	@Override
	public void onDraw(Canvas canvas) {
		synchronized(lock) {
			if (!images.scaled) {
				images.scaleBitmaps();
				gameBoard.initializeImages();
				localPlayer.getPlayerOrientation(true);
			}
			//canvas.drawColor(Color.BLACK);
	
			Paint paint = new Paint();
			paint.setStrokeWidth(1);
			
			//Draw local Board
			for(int row = localPlayer.getPosition().y-screen.getVerticalRadius()-2; row <= localPlayer.getPosition().y+screen.getVerticalRadius()+2; row++)
			{
				for(int column = localPlayer.getPosition().x-screen.getHorizontalRadius()-2; column <= localPlayer.getPosition().x+screen.getHorizontalRadius()+2; column++)
				{
					//Check for out of bounds
					if(		(row) < 0 ||
							(row) > (gameBoard.width-1) ||
							(column) < 0 ||
							(column) > (gameBoard.height-1)	)
					{
						//Out of Bounds
						fillCell(canvas, paint, Color.BLACK, true, new Point(column, row), true);
					}
					else { 
						drawImage(images.grasses.get(0), canvas, paint, new Point(column, row), true);
						drawImage(gameBoard.imageAt(new Point(column, row)), canvas, paint, new Point(column, row), true);
					}
					
					
				}
			}
			
			for (Player p : gameManager.getPlayers())
				if (!p.isLocal())
					drawImage(p.getPlayerOrientation(false), canvas, paint, p);
				else {
					drawImage(p.getPlayerOrientation(false), canvas, paint, p.getPosition(), false);
				}
			
			if(!localPlayer.getHasXRayVision()) {
				//Draw Shadows
				paint.setStyle(Paint.Style.FILL);
				paint.setColor(Color.GRAY);
				for(int i = 0; i < rayCaster.getNumberOfShadows(); i++){
					canvas.drawPath(rayCaster.getShadowPolygon(i), paint);
				}
			}
						
			//Draw universally visible objects
			for(int row = localPlayer.getPosition().y-screen.getVerticalRadius()-2; row <= localPlayer.getPosition().y+screen.getVerticalRadius()+2; row++)
			{
				for(int column = localPlayer.getPosition().x-screen.getHorizontalRadius()-2; column <= localPlayer.getPosition().x+screen.getHorizontalRadius()+2; column++)
				{
					//Check for out of bounds
					if(		(row) < 0 ||
							(row) > (gameBoard.width-1) ||
							(column) < 0 ||
							(column) > (gameBoard.height-1)	)
					{
						//Out of Bounds
						fillCell(canvas, paint, Color.BLACK, true, new Point(column, row), true);
					}else{
						//In Bounds
						if(gameBoard.isUniversallyVisible(column, row))
						{
							drawImage(gameBoard.imageAt(new Point(column, row)), canvas, paint, new Point(column, row), true);
						}
					}
					
					
				}
			}
			
			//Draw Desired Cell
			if (localPlayer.shouldDrawTarget())
				drawImage(images.target, canvas, paint, localPlayer.getTargetLocation(), true);
						
			//Print Score
			paint.setColor(Color.WHITE);
			paint.setTextSize(20);
			canvas.drawText("Score : " + localPlayer.getScore(), 10, 20, paint);
			canvas.drawText(gameManager.timeRemainingString(), 10, 40, paint);
			
			if(!isGameStarted){
				//PrintCountdown
				paint.setColor(Color.WHITE);
				paint.setTextSize(100);
				canvas.drawText(startGameTimerString, getWidth()/2, getHeight()/2, paint);
			}
		}
	}
	
	/**
	 * Fills a cell.
	 * @param canvas - The canvas we're drawing on.
	 * @param paint - The paint we're using.
	 * @param color - The color to fill with.
	 * @param rectangle - Whether to draw a rectangle or a circle.
	 * @param position - The cell to fill.
	 * @param graduate - Whether or not to graduate movement.
	 */
	private void fillCell(Canvas canvas, Paint paint, int color, boolean rectangle, Point position, boolean graduate) {
		paint.setColor(color);
		Point topLeftCorner = screen.absoluteCellToStartPixel(position, localPlayer, getWidth(), getHeight(), graduate);
		Point bottomRightCorner = screen.absoluteCellToEndPixel(position, localPlayer, getWidth(), getHeight(), graduate);
		if (rectangle)
			canvas.drawRect(topLeftCorner.x, topLeftCorner.y, bottomRightCorner.x, bottomRightCorner.y, paint);
		else {
			RectF ovalBounds = new RectF(topLeftCorner.x,topLeftCorner.y,bottomRightCorner.x,bottomRightCorner.y);
			canvas.drawOval(ovalBounds, paint);
		}
	}
	
	/**
	 * Draws an image.
	 * @param image - The image.
	 * @param canvas - The canvas we're using.
	 * @param paint - The paint we're using.
	 * @param position - The cell at which to draw the image.
	 * @param graduate - Whether or not to graduate the image.
	 */
	private void drawImage(Bitmap image, Canvas canvas, Paint paint, Point position, boolean graduate) {
		Point corner = screen.absoluteCellToStartPixel(position, localPlayer, getWidth(), getHeight(), graduate);
		canvas.drawBitmap(image, corner.x, corner.y, paint);
	}
	
	/**
	 * Draws an image.
	 * @param image - The image.
	 * @param canvas - The canvas we're using.
	 * @param paint - The paint we're using.
	 * @param aiPlayer - The aiPlayer to draw.
	 */
	private void drawImage(Bitmap image, Canvas canvas, Paint paint, Player aiPlayer) {
		Point corner = screen.absoluteCellToStartPixel(aiPlayer.getPosition(), localPlayer, getWidth(), getHeight(), true);
		corner = screen.adjustPlayer(corner, aiPlayer, getWidth(), getHeight());
		
		canvas.drawBitmap(image, corner.x, corner.y, paint);
	}

	/**
	 * Image data.
	 * @author Milo
	 */
	public class Images {
		public Bitmap player = fetch(R.drawable.player2);
		public Bitmap treasureCoin = fetch(R.drawable.treasure_coin);
		public Bitmap treasureJewel = fetch(R.drawable.treasure_jewel);
		public Bitmap treasureChest = fetch(R.drawable.treasure_chest);
		public Bitmap target = fetch(R.drawable.target);
		public Bitmap stone = fetch(R.drawable.stone);
		public Bitmap stone2 = fetch(R.drawable.stone2);
		public Bitmap stone3 = fetch(R.drawable.stone3);
		public Bitmap stone4 = fetch(R.drawable.stone4);
		public Bitmap grass = fetch(R.drawable.grass);
		public Bitmap grass2 = fetch(R.drawable.grass2);
		public Bitmap grass3 = fetch(R.drawable.grass3);
		public Bitmap grass4 = fetch(R.drawable.grass4);
		public Bitmap doorClosed = fetch(R.drawable.door_closed);
		public Bitmap doorOpen = fetch(R.drawable.door_open);
		public Bitmap speedBoost = fetch(R.drawable.speed);
		public Bitmap xrayVision = fetch(R.drawable.xray);
		public Bitmap tree = fetch(R.drawable.tree);
		public Bitmap tree2 = fetch(R.drawable.tree2);
		public Bitmap tree3 = fetch(R.drawable.tree3);
		public Bitmap tree4 = fetch(R.drawable.tree4);
		public Bitmap rock = fetch(R.drawable.rocks);
		public Bitmap rock2 = fetch(R.drawable.rocks2);
		public Bitmap rock3 = fetch(R.drawable.rocks3);
		public Bitmap rock4 = fetch(R.drawable.rocks4);
		public Bitmap water = fetch(R.drawable.water);
		public Bitmap water2 = fetch(R.drawable.water2);
		public Bitmap water3 = fetch(R.drawable.water3);
		public Bitmap water4 = fetch(R.drawable.water4);
		public Bitmap water5 = fetch(R.drawable.water5);
		public Bitmap water6 = fetch(R.drawable.water6);
		public Bitmap bridgeH = fetch(R.drawable.bridge_h);
		public Bitmap bridgeV = fetch(R.drawable.bridge_v);
		public Bitmap wall = fetch(R.drawable.wall);
		public Bitmap playerBackLeft = fetch(R.drawable.character_back_left);
		public Bitmap playerBackLeftBoots = fetch(R.drawable.character_back_left_boots);
		public Bitmap playerBackLeftXRay = fetch(R.drawable.character_back_left_xray);
		public Bitmap playerBackLeftXRayBoots = fetch(R.drawable.character_back_left_xray_boots);
		public Bitmap playerBackRight = fetch(R.drawable.character_back_right);
		public Bitmap playerBackRightBoots = fetch(R.drawable.character_back_right_boots);
		public Bitmap playerBackRightXRay = fetch(R.drawable.character_back_right_xray);
		public Bitmap playerBackRightXRayBoots = fetch(R.drawable.character_back_right_xray_boots);
		public Bitmap playerFrontLeft = fetch(R.drawable.character_front_left);
		public Bitmap playerFrontLeftBoots = fetch(R.drawable.character_front_left_boots);
		public Bitmap playerFrontLeftXRay = fetch(R.drawable.character_front_left_xray);
		public Bitmap playerFrontLeftXRayBoots = fetch(R.drawable.character_front_left_xray_boots);
		public Bitmap playerFrontRight = fetch(R.drawable.character_front_right);
		public Bitmap playerFrontRightBoots = fetch(R.drawable.character_front_right_boots);
		public Bitmap playerFrontRightXRay = fetch(R.drawable.character_front_right_xray);
		public Bitmap playerFrontRightXRayBoots = fetch(R.drawable.character_front_right_xray_boots);
		
		public Bitmap player2BackLeft = fetch(R.drawable.character2_back_left);
		public Bitmap player2BackLeftBoots = fetch(R.drawable.character2_back_left_boots);
		public Bitmap player2BackLeftXRay = fetch(R.drawable.character2_back_left_xray);
		public Bitmap player2BackLeftXRayBoots = fetch(R.drawable.character2_back_left_xray_boots);
		public Bitmap player2BackRight = fetch(R.drawable.character2_back_right);
		public Bitmap player2BackRightBoots = fetch(R.drawable.character2_back_right_boots);
		public Bitmap player2BackRightXRay = fetch(R.drawable.character2_back_right_xray);
		public Bitmap player2BackRightXRayBoots = fetch(R.drawable.character2_back_right_xray_boots);
		public Bitmap player2FrontLeft = fetch(R.drawable.character2_front_left);
		public Bitmap player2FrontLeftBoots = fetch(R.drawable.character2_front_left_boots);
		public Bitmap player2FrontLeftXRay = fetch(R.drawable.character2_front_left_xray);
		public Bitmap player2FrontLeftXRayBoots = fetch(R.drawable.character2_front_left_xray_boots);
		public Bitmap player2FrontRight = fetch(R.drawable.character2_front_right);
		public Bitmap player2FrontRightBoots = fetch(R.drawable.character2_front_right_boots);
		public Bitmap player2FrontRightXRay = fetch(R.drawable.character2_front_right_xray);
		public Bitmap player2FrontRightXRayBoots = fetch(R.drawable.character2_front_right_xray_boots);
		
		public Bitmap player3BackLeft = fetch(R.drawable.character3_back_left);
		public Bitmap player3BackLeftBoots = fetch(R.drawable.character3_back_left_boots);
		public Bitmap player3BackLeftXRay = fetch(R.drawable.character3_back_left_xray);
		public Bitmap player3BackLeftXRayBoots = fetch(R.drawable.character3_back_left_xray_boots);
		public Bitmap player3BackRight = fetch(R.drawable.character3_back_right);
		public Bitmap player3BackRightBoots = fetch(R.drawable.character3_back_right_boots);
		public Bitmap player3BackRightXRay = fetch(R.drawable.character3_back_right_xray);
		public Bitmap player3BackRightXRayBoots = fetch(R.drawable.character3_back_right_xray_boots);
		public Bitmap player3FrontLeft = fetch(R.drawable.character3_front_left);
		public Bitmap player3FrontLeftBoots = fetch(R.drawable.character3_front_left_boots);
		public Bitmap player3FrontLeftXRay = fetch(R.drawable.character3_front_left_xray);
		public Bitmap player3FrontLeftXRayBoots = fetch(R.drawable.character3_front_left_xray_boots);
		public Bitmap player3FrontRight = fetch(R.drawable.character3_front_right);
		public Bitmap player3FrontRightBoots = fetch(R.drawable.character3_front_right_boots);
		public Bitmap player3FrontRightXRay = fetch(R.drawable.character3_front_right_xray);
		public Bitmap player3FrontRightXRayBoots = fetch(R.drawable.character3_front_right_xray_boots);
		
		public Bitmap lockedDoorClosed = fetch(R.drawable.locked_door_closed);
		public Bitmap lockedDoorOpen = fetch(R.drawable.locked_door_open);
		public Bitmap key = fetch(R.drawable.key);
		
		
		
		public List<Bitmap> stones = Arrays.asList(stone, stone2, stone3, stone4);
		public List<Bitmap> grasses = Arrays.asList(grass, grass2, grass3, grass4);
		public List<Bitmap> trees = Arrays.asList(tree, tree2, tree3, tree4);
		public List<Bitmap> rocks = Arrays.asList(rock, rock2, rock3, rock4);
		public List<Bitmap> waters = Arrays.asList(water, water2, water3, water4);
		public List<Bitmap> playerImages = Arrays.asList(	playerBackLeft, playerBackLeftBoots, playerBackLeftXRay, playerBackLeftXRayBoots,
															playerBackRight, playerBackRightBoots, playerBackRightXRay, playerBackRightXRayBoots,
															playerFrontLeft, playerFrontLeftBoots, playerFrontLeftXRay, playerFrontLeftXRayBoots,
															playerFrontRight, playerFrontRightBoots, playerFrontRightXRay, playerFrontRightXRayBoots,
															player2BackLeft, player2BackLeftBoots, player2BackLeftXRay, player2BackLeftXRayBoots,
															player2BackRight, player2BackRightBoots, player2BackRightXRay, player2BackRightXRayBoots,
															player2FrontLeft, player2FrontLeftBoots, player2FrontLeftXRay, player2FrontLeftXRayBoots,
															player2FrontRight, player2FrontRightBoots, player2FrontRightXRay, player2FrontRightXRayBoots,
															player3BackLeft, player3BackLeftBoots, player3BackLeftXRay, player3BackLeftXRayBoots,
															player3BackRight, player3BackRightBoots, player3BackRightXRay, player3BackRightXRayBoots,
															player3FrontLeft, player3FrontLeftBoots, player3FrontLeftXRay, player3FrontLeftXRayBoots,
															player3FrontRight, player3FrontRightBoots, player3FrontRightXRay, player3FrontRightXRayBoots);
		
		/** Whether or not we've scaled the bitmaps yet. **/
		private boolean scaled = false;
		
		/**
		 * Scales all the bitmaps.
		 */
		public void scaleBitmaps() {
			player = scale(player);
			treasureCoin = scale(treasureCoin);
			treasureJewel = scale(treasureJewel);
			treasureChest = scale(treasureChest);
			target = scale(target);
			doorClosed = scale(doorClosed);
			doorOpen = scale(doorOpen);
			speedBoost = scale(speedBoost);
			xrayVision = scale(xrayVision);
			bridgeH = scale(bridgeH);
			bridgeV = scale(bridgeV);
			wall = scale(wall);
			
			stones = scale(stones);
			grasses = scale(grasses);
			trees = scale(trees);
			rocks = scale(rocks);
			waters = scale(waters);
			
			playerImages = scale(playerImages);
			
			lockedDoorClosed = scale(lockedDoorClosed);
			lockedDoorOpen = scale(lockedDoorOpen);
			key = scale(key);
			
			scaled = true;
		}
		
		/**
		 * Scales a bitmap.
		 * @param bitmap - The bitmap to scale.
		 * @return the scaled bitmap.
		 */
		private Bitmap scale(Bitmap bitmap) {
			return Bitmap.createScaledBitmap(bitmap, getWidth()/(2*screen.getHorizontalRadius()+1), getHeight()/(2*screen.getVerticalRadius()+1), false);
		}
		
		/**
		 * Scales a list of bitmaps.
		 * @param bitmaps - The list of bitmaps.
		 * @return a list of scaled bitmaps.
		 */
		private List<Bitmap> scale(List<Bitmap> bitmaps) {
			List<Bitmap> scaled = new LinkedList<Bitmap>();
			for (Bitmap bitmap : bitmaps)
				scaled.add(scale(bitmap));
			return scaled;
		}
		
		/**
		 * Decodes an image resource.
		 * @param resource - the resources.
		 * @return a bitmap image.
		 */
		private Bitmap fetch(int resource) {
			return BitmapFactory.decodeResource(getResources(), resource);
		}
		
		/** @return if we've scaled the images. **/
		public boolean scaled() { return scaled; }
	}
	
}
