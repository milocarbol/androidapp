package com.eece381.androidcode.gui;

import android.graphics.Point;
import android.util.Log;

import com.eece381.androidcode.actor.Player;

/**
 * Screen that moves with the player.
 * @author Milo
 */
public class MovingScreen implements IScreen {
	
	/** The number of cells visible on each side of the player **/
	private int horizontalVisibility;
	
	/** The number of cells visible above and below the player **/
	private int verticalVisibility;
	
	/** The total number of visible horizontal cells **/
	private int totalHorizontalCells;
	
	/** The total number of visible vertical cells **/
	private int totalVerticalCells;
	
	/** The height of the game board **/
	private int boardHeight;
	
	/** The width of the game board **/
	private int boardWidth;
	
	/** The local player **/
	private Player player;
	
	/**
	 * Creates a new Moving screen.
	 * @param player - The local player to centre on.
	 * @param boardWidth - The width of the board.
	 * @param boardHeight - The height of the board.
	 * @param horizontalVisibility - The number of cells visible on each side of the player.
	 * @param verticalVisibility - The number of cells visible above and below the player.
	 */
	public MovingScreen(Player player, int boardWidth, int boardHeight, int horizontalVisibility, int verticalVisibility) {
		this.player = player;
		this.boardWidth = boardWidth;
		this.boardHeight = boardHeight;
		this.horizontalVisibility = horizontalVisibility;
		this.verticalVisibility = verticalVisibility;
		this.totalHorizontalCells = horizontalVisibility * 2 + 1;
		this.totalVerticalCells = verticalVisibility * 2 + 1;
		
	}
	
	@Override
	public int leftBound() {
		return player.getPosition().x - horizontalVisibility;
	}

	@Override
	public int rightBound() {
		return player.getPosition().x + horizontalVisibility;
	}

	@Override
	public int topBound() {
		return player.getPosition().y - verticalVisibility;
	}

	@Override
	public int bottomBound() {
		return player.getPosition().y + verticalVisibility;
	}

	/**
	 * Validates a cell to see if it's off the game board.
	 * @param tapped - The cell that was tapped by the player.
	 * @return the original point if valid, otherwise a corrected point.
	 */
	public Point validate(Point tapped) {
		Point valid = new Point();
		
		if (tapped.x < 0)
			valid.x = 0;
		else if (tapped.x >= boardWidth)
			valid.x = boardWidth - 1;
		else
			valid.x = tapped.x;
		
		if (tapped.y < 0)
			valid.y = 0;
		else if (tapped.y >= boardHeight)
			valid.y = boardHeight - 1;
		else
			valid.y = tapped.y;
		
		return valid;
	}
	
	/**
	 * Computes the game board cell corresponding to pixel coordinates.
	 * @param screenCentre - The cell the screen is centred on.
	 * @param pixelX - The x-coordinate of the pixel.
	 * @param pixelY - The y-coordinate of the pixel.
	 * @param screenWidth - The width of the screen, in pixels.
	 * @param screenHeight - The height of the screen, in pixels.
	 * @return the cell coordinates.
	 */
	public Point pixelToAbsoluteCell(Point screenCentre, int pixelX, int pixelY, int screenWidth, int screenHeight)
	{
		Point cell = new Point();
		cell.x = screenCentre.x + (int)(pixelX/(screenWidth/totalHorizontalCells)) - horizontalVisibility;	
		cell.y = screenCentre.y + (int)(pixelY/(screenHeight/totalVerticalCells)) - verticalVisibility;
		return cell;
	}
	
	/**
	 * Computes the top-left pixel coordinates of a cell.
	 * @param cell - The cell coordinates.
	 * @param player - the player.
	 * @param screenWidth - The width of the screen, in pixels.
	 * @param screenHeight - The height of the screen, in pixels.
	 * @param shouldAdjust - Whether or not the cell should slide based on graduated movement
	 * @return the pixel coordinates of the top-left of the cell.
	 */
	private Point absoluteCellToPixel(Point cell, Player player, int screenWidth, int screenHeight, boolean shouldAdjust)
	{
		Point pixel = new Point();
		int adjustmentX = 0;
		int adjustmentY = 0;
			
		if(shouldAdjust)
		{
			double fractionOfCellX = (((double)player.getActivatedPosition().x - player.getPosition().x)/player.getSpeed())*player.getTick();
			double fractionOfCellY = (((double)player.getActivatedPosition().y - player.getPosition().y)/player.getSpeed())*player.getTick();
		
			adjustmentX = (int)(fractionOfCellX*(screenWidth/totalHorizontalCells));
			adjustmentY = (int)(fractionOfCellY*(screenHeight/totalVerticalCells));
		}
		
		pixel.x = (cell.x-player.getPosition().x+horizontalVisibility)*(screenWidth/totalHorizontalCells)-adjustmentX;
		pixel.y = (cell.y-player.getPosition().y+verticalVisibility)*(screenHeight/totalVerticalCells)-adjustmentY;
		return pixel;
	}
	
	/**
	 * Computes the bottom-right pixel coordinates of a cell.
	 * @param cell - The cell coordinates.
	 * @param player - the Player.
	 * @param screenWidth - The width of the screen, in pixels.
	 * @param screenHeight - The height of the screen, in pixels.
	 * @param shouldAdjust - Whether or not the cell should slide based on graduated movement
	 * @return the pixel coordinates of the bottom-right of the cell.
	 */
	public Point absoluteCellToStartPixel(Point cell, Player player, int screenWidth, int screenHeight, boolean shouldAdjust)
	{
		Point end = absoluteCellToPixel(cell, player, screenWidth, screenHeight, shouldAdjust);
		return end;
	}
	
	/**
	 * Computes the bottom-right pixel coordinates of a cell.
	 * @param cell - The cell coordinates.
	 * @param player - the Player.
	 * @param screenWidth - The width of the screen, in pixels.
	 * @param screenHeight - The height of the screen, in pixels.
	 * @param shouldAdjust - Whether or not the cell should slide based on graduated movement
	 * @return the pixel coordinates of the bottom-right of the cell.
	 */
	public Point absoluteCellToEndPixel(Point cell, Player player, int screenWidth, int screenHeight, boolean shouldAdjust)
	{
		Point end = absoluteCellToPixel(new Point(cell.x+1, cell.y+1), player, screenWidth, screenHeight, shouldAdjust);
		return end;
	}
	
	/**
	 * Computes the Top Left pixel coordinates of a cell.
	 * @param cell - The cell coordinates.
	 * @param player - the Player.
	 * @param screenWidth - The width of the screen, in pixels.
	 * @param screenHeight - The height of the screen, in pixels.
	 * @param shouldAdjust - Whether or not the cell should slide based on graduated movement
	 * @return the pixel coordinates of the bottom-right of the cell.
	 */
	public Point absoluteCellToTopLeftPixel(Point cell, Player player, int screenWidth, int screenHeight, boolean shouldAdjust)
	{
		Point end = absoluteCellToPixel(cell, player, screenWidth, screenHeight, shouldAdjust);
		return end;
	}
	
	/**
	 * Computes the top right pixel coordinates of a cell.
	 * @param cell - The cell coordinates.
	 * @param player - the Player.
	 * @param screenWidth - The width of the screen, in pixels.
	 * @param screenHeight - The height of the screen, in pixels.
	 * @param shouldAdjust - Whether or not the cell should slide based on graduated movement
	 * @return the pixel coordinates of the bottom-right of the cell.
	 */
	public Point absoluteCellToTopRightPixel(Point cell, Player player, int screenWidth, int screenHeight, boolean shouldAdjust)
	{
		Point end = absoluteCellToPixel(new Point(cell.x+1,cell.y), player, screenWidth, screenHeight, shouldAdjust);
		end.x--;
		return end;
	}
	
	/**
	 * Computes the bottom left pixel coordinates of a cell.
	 * @param cell - The cell coordinates.
	 * @param player - the Player.
	 * @param screenWidth - The width of the screen, in pixels.
	 * @param screenHeight - The height of the screen, in pixels.
	 * @param shouldAdjust - Whether or not the cell should slide based on graduated movement
	 * @return the pixel coordinates of the bottom-right of the cell.
	 */
	public Point absoluteCellToBottomLeftPixel(Point cell, Player player, int screenWidth, int screenHeight, boolean shouldAdjust)
	{
		Point end = absoluteCellToPixel(new Point(cell.x,cell.y+1), player, screenWidth, screenHeight, shouldAdjust);
		end.y--;
		return end;
	}
	
	/**
	 * Computes the bottom right pixel coordinates of a cell.
	 * @param cell - The cell coordinates.
	 * @param player - the Player.
	 * @param screenWidth - The width of the screen, in pixels.
	 * @param screenHeight - The height of the screen, in pixels.
	 * @param shouldAdjust - Whether or not the cell should slide based on graduated movement
	 * @return the pixel coordinates of the bottom-right of the cell.
	 */
	public Point absoluteCellToBottomRightPixel(Point cell, Player player, int screenWidth, int screenHeight, boolean shouldAdjust)
	{
		Point end = absoluteCellToPixel(new Point(cell.x+1,cell.y+1), player, screenWidth, screenHeight, shouldAdjust);
		end.x--;
		end.y--;
		return end;
	}
	
	/**
	 * Computes the center pixel coordinates of a cell.
	 * @param cell - The cell coordinates.
	 * @param player - the Player.
	 * @param screenWidth - The width of the screen, in pixels.
	 * @param screenHeight - The height of the screen, in pixels.
	 * @param shouldAdjust - Whether or not the cell should slide based on graduated movement
	 * @return the pixel coordinates of the bottom-right of the cell.
	 */
	public Point absoluteCellToCenterPixel(Point cell, Player player, int screenWidth, int screenHeight, boolean shouldAdjust)
	{
		Point end = absoluteCellToPixel(new Point(cell.x,cell.y), player, screenWidth, screenHeight, shouldAdjust);
		end.x += ((screenWidth/totalHorizontalCells)/2);
		end.y += ((screenHeight/totalVerticalCells)/2);
		return end;
	}
	
	public Point adjustPlayer(Point pixel, Player aiPlayer, int screenWidth, int screenHeight) {
		Point newPixel = new Point(pixel);
		
		int directionX = aiPlayer.getActivatedPosition().x - aiPlayer.getPosition().x;
		int directionY = aiPlayer.getActivatedPosition().y - aiPlayer.getPosition().y;
		double sizeOfFractionX = ((double)directionX)/aiPlayer.getSpeed();
		double sizeOfFractionY = ((double)directionY)/aiPlayer.getSpeed();
		double fractionOfCellX = sizeOfFractionX*aiPlayer.getTick();
		double fractionOfCellY = sizeOfFractionY*aiPlayer.getTick();
		
		int cellWidth = screenWidth/totalHorizontalCells;
		int cellHeight = screenHeight/totalVerticalCells;
		
		int adjustmentX = (int)(fractionOfCellX*cellWidth);
		int adjustmentY = (int)(fractionOfCellY*cellHeight);
		
		newPixel.x += adjustmentX;
		newPixel.y += adjustmentY;
		return newPixel;
	}
	
	/**
	 * @return number of visible horizontal cells
	 */
	public int getWidthInCells()
	{
		return totalHorizontalCells;
	}
	
	/**
	 * @return number of visible vertical cells
	 */
	public int getHeightInCells()
	{
		return totalVerticalCells;
	}
	
	/**
	 * @return the number of cells between the center cell and the side of the screen
	 */
	public int getHorizontalRadius()
	{
		return horizontalVisibility;
	}
	
	/**
	 * @return the number of cells between the center cell and the top of the screen
	 */
	public int getVerticalRadius()
	{
		return verticalVisibility;
	}
}
