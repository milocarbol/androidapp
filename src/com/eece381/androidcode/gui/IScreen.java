package com.eece381.androidcode.gui;

/**
 * Controlls the player's screen.
 * @author Milo
 */
public interface IScreen {

	/** @return the leftmost visible column **/
	public int leftBound();
	
	/** @return the rightmost visible column **/
	public int rightBound();
	
	/** @return the topmost visible row **/
	public int topBound();
	
	/** @return the bottommost visible row **/
	public int bottomBound();
}
