package com.eece381.androidcode.gui;

import android.graphics.Bitmap;
import android.graphics.Point;

import com.eece381.androidcode.game.GameBoard;

/**
 * Grid of images corresponding to objects on the game board.
 * @author Milo
 */
public class ImageBoard {

	/** The grid of images. **/
	private Bitmap[][] imageGrid;
	
	/** The images we can use. **/
	private GameView.Images images;
	
	/**
	 * Creates a new image board.
	 * @param gameBoard - The game board to correspond to.
	 * @param images - The images we can use.
	 */
	public ImageBoard(GameBoard gameBoard, GameView.Images images) {
		if (!images.scaled())
			images.scaleBitmaps();
		this.images = images;
		this.imageGrid = setImages(gameBoard);
	}
	
	/**
	 * Gets the image at a given cell.
	 * @param cell - The cell.
	 * @return the image for that cell.
	 */
	public Bitmap imageAt(Point cell) {
		return imageGrid[cell.x][cell.y];
	}
	
	/**
	 * Sets the images on the board according to the game board.
	 * @param gameBoard - The game board.
	 * @return the grid of images.
	 */
	private Bitmap[][] setImages(GameBoard gameBoard) {
		imageGrid = new Bitmap[gameBoard.width][gameBoard.height];
		for (int column = 0; column < gameBoard.width; column++)
			for (int row = 0; row < gameBoard.height; row++) {
				GameBoard.CELL_OBJECT type = gameBoard.getCellValue(column, row);
				if (type == GameBoard.CELL_OBJECT.WALL)
					imageGrid[column][row] = images.wall;
				else if (type == GameBoard.CELL_OBJECT.TREASURE_COIN)
					imageGrid[column][row] = images.treasureCoin;
				else if (type == GameBoard.CELL_OBJECT.TREASURE_JEWEL)
					imageGrid[column][row] = images.treasureJewel;
				else if (type == GameBoard.CELL_OBJECT.TREASURE_CHEST)
					imageGrid[column][row] = images.treasureChest;
				else if (type == GameBoard.CELL_OBJECT.DOOR_CLOSED)
					imageGrid[column][row] = images.doorClosed;
				else if (type == GameBoard.CELL_OBJECT.DOOR_OPEN)
					imageGrid[column][row] = images.doorOpen;
				else if (type == GameBoard.CELL_OBJECT.SPEED_BOOST)
					imageGrid[column][row] = images.speedBoost;
				else if (type == GameBoard.CELL_OBJECT.XRAY_VISION)
					imageGrid[column][row] = images.xrayVision;
				else if (type == GameBoard.CELL_OBJECT.TREE)
					imageGrid[column][row] = randomTree(images);
				else if (type == GameBoard.CELL_OBJECT.ROCK)
					imageGrid[column][row] = randomRock(images);
				else if (type == GameBoard.CELL_OBJECT.WATER)
					imageGrid[column][row] = randomWater(images);
				else if (type == GameBoard.CELL_OBJECT.BRIDGE)
					imageGrid[column][row] = orientedBridge(images, column, row, gameBoard);
				else if (type == GameBoard.CELL_OBJECT.LOCKED_DOOR_CLOSED)
					imageGrid[column][row] = images.lockedDoorClosed;
				else if (type == GameBoard.CELL_OBJECT.LOCKED_DOOR_OPEN)
					imageGrid[column][row] = images.lockedDoorOpen;
				else if (type == GameBoard.CELL_OBJECT.KEY)
					imageGrid[column][row] = images.key;
				else
					imageGrid[column][row] = randomGrass(images);
			}
		return imageGrid;
	}
	
	/**
	 * Generates a random stone image from the set of stone images.
	 * @param images - The images we can use.
	 * @return a random stone image.
	 */
	private Bitmap randomStone(GameView.Images images) {
		return images.stones.get((int)(Math.random()*images.stones.size()));
	}
	
	/**
	 * Generates a random grass image from the set of grass images.
	 * @param images - The images we can use.
	 * @return a random grass image.
	 */
	private Bitmap randomGrass(GameView.Images images) {
		return images.grasses.get((int)(Math.random()*images.grasses.size()));
	}
	
	/**
	 * Generates a random tree image from the set of tree images.
	 * @param images - The images we can use.
	 * @return a random tree image.
	 */
	private Bitmap randomTree(GameView.Images images) {
		return images.trees.get((int)(Math.random()*images.trees.size()));
	}

	/**
	 * Generates a random rock pile image from the set of rock pile images.
	 * @param images - The images we can use.
	 * @return a random rock pile image.
	 */
	private Bitmap randomRock(GameView.Images images) {
		return images.rocks.get((int)(Math.random()*images.rocks.size()));
	}
	
	/**
	 * Generates a random water image from the set of water images.
	 * @param images - The images we can use.
	 * @return a random water image.
	 */
	private Bitmap randomWater(GameView.Images images) {
		return images.waters.get((int)(Math.random()*images.waters.size()));
	}

	/**
	 * Generates a bridge image oriented to the water around it.
	 * @param images - The images we can use.
	 * @return a bridge image, oriented correctly.
	 */
	private Bitmap orientedBridge(GameView.Images images, int column, int row, GameBoard gameBoard) {
		if (gameBoard.getCellValue(column-1, row) == GameBoard.CELL_OBJECT.WATER
				|| gameBoard.getCellValue(column+1, row) == GameBoard.CELL_OBJECT.WATER)
			return images.bridgeV;
		return images.bridgeH;
	}
	
	/**
	 * Clears the image from a cell.
	 * @param cell - The cell to clear.
	 */
	public void clear(Point cell) {
		imageGrid[cell.x][cell.y] = images.grass;
	}
	
	/**
	 * Changes a cell to be an open door.
	 * Note: This does no checking to see if the cell was a closed door.
	 * @param cell - The cell.
	 */
	public void openDoor(Point cell) {
		imageGrid[cell.x][cell.y] = images.doorOpen;
	}
	
	/**
	 * Changes a cell to be a closed door.
	 * Note: This does no checking to see if the cell was an open door.
	 * @param cell - The cell.
	 */
	public void closeDoor(Point cell) {
		imageGrid[cell.x][cell.y] = images.doorClosed;
	}
	
	/**
	 * changes cell to locked door open
	 * @param cell - The cell
	 */
	public void openLockedDoor(Point cell) {
		imageGrid[cell.x][cell.y] = images.lockedDoorOpen;
	}
	
	/**
	 * changes cell to locked door closed
	 * @param cell - The cell
	 */
	public void closeLockedDoor(Point cell) {
		imageGrid[cell.x][cell.y] = images.lockedDoorClosed;
	}
	
	public void addImage(Point cell, Bitmap image) {
		imageGrid[cell.x][cell.y] = image;
	}
}
