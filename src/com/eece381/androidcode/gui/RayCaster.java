package com.eece381.androidcode.gui;

import java.util.LinkedList;
import java.util.List;

import android.graphics.Path;
import android.graphics.Point;

import com.eece381.androidcode.actor.Player;
import com.eece381.androidcode.game.GameBoard;
/**
 * Class responsible for determining the visibility field of the player
 * @author Nathan
 *
 */
public class RayCaster {
	private GameView gameView;
	private List<Path> notVisiblePolygons = new LinkedList<Path>();
	private MovingScreen screen;
	private Player player;
	private GameBoard gameBoard;
	
	/**
	 * Creates a RayCaster Object
	 * @param gameView - View containing the game
	 * @param screen - The screen object
	 * @param player - The local player
	 * @param gameBoard - The game board
	 */
	public RayCaster(GameView gameView, MovingScreen screen, Player player, GameBoard gameBoard) {
		this.gameView = gameView;
		this.screen = screen;
		this.player = player;
		this.gameBoard = gameBoard;
	}
	
	public void runRayCaster() {
		//Reset the blocked region list
		notVisiblePolygons.clear();
		//Check local board
		for(int row = player.getPosition().y-screen.getVerticalRadius()-1; row <= player.getPosition().y+screen.getVerticalRadius()+1; row++)
		{
			for(int column = player.getPosition().x-screen.getHorizontalRadius()-1; column <= player.getPosition().x+screen.getHorizontalRadius()+1; column++)
			{
				//Check for out of bounds
				if(		(row) < 0 ||
						(row) > (gameBoard.width-1) ||
						(column) < 0 ||
						(column) > (gameBoard.height-1)	)
				{
					continue;
				}else if(gameBoard.isVisionBlocking(column, row)){
					//Vision Blocking Object
					notVisiblePolygons.add(calculateRadialRegion(player, column, row, screen.getHorizontalRadius(), screen.getVerticalRadius()));
				}
			}
		}
	}
	
	
	private Path finishCalculatingPolygon(Point destEndPixel, Point destStartPixel, Point sourceCenterPixel, List<Point> polyPoints, Point firstCorner, Point secondCorner) {
		
		//Calculate line extension
		float endPointSlope = (float)(destEndPixel.y - sourceCenterPixel.y)/(float)(destEndPixel.x - sourceCenterPixel.x);
		float endPointYIntercept = destEndPixel.y - (destEndPixel.x*endPointSlope);
		
		float startPointSlope = (float)(destStartPixel.y - sourceCenterPixel.y)/(float)(destStartPixel.x - sourceCenterPixel.x);
		float startPointYIntercept = destStartPixel.y - (destStartPixel.x*startPointSlope);
		
		if(firstCorner.equals(secondCorner)){
			//Diagonally blocking
			
			//end line X = firstCorner.x (hits one of the side walls)
			if(((endPointSlope*firstCorner.x) + endPointYIntercept) >= 0 && ((endPointSlope*firstCorner.x) + endPointYIntercept) < gameView.getHeight()){
				if(endPointSlope == 0){
					polyPoints.add(new Point(firstCorner.x,(int)sourceCenterPixel.y));
				}else{
					polyPoints.add(new Point(firstCorner.x,(int)((endPointSlope*firstCorner.x) + endPointYIntercept)));
				}
					
				//start line X = firstCorner.x (hits same side wall)
				if(((startPointSlope*firstCorner.x) + startPointYIntercept) >= 0 && ((startPointSlope*firstCorner.x) + startPointYIntercept) < gameView.getHeight()){
					if(startPointSlope == 0){
						polyPoints.add(new Point(firstCorner.x,(int)sourceCenterPixel.y));
					}else{
						//if both extension points hit the same wall don't need corner
						polyPoints.add(new Point(firstCorner.x,(int)((startPointSlope*firstCorner.x) + startPointYIntercept)));
					}
				}else{
					//start line Y = firstCorner.y (hits the top or bottom wall)
					//if extension points are on different walls we need to add the corner
					polyPoints.add(new Point(firstCorner.x,firstCorner.y));
					if(startPointSlope == Float.POSITIVE_INFINITY || startPointSlope == Float.NEGATIVE_INFINITY){
						//Directly below or above
						polyPoints.add(new Point((int)sourceCenterPixel.x,firstCorner.y));
					}else{
						polyPoints.add(new Point((int)((firstCorner.y-startPointYIntercept)/startPointSlope),firstCorner.y));
					}
				}
			}else{
				if(endPointSlope == Float.POSITIVE_INFINITY || endPointSlope == Float.NEGATIVE_INFINITY){
					//Directly below or above
					polyPoints.add(new Point((int)sourceCenterPixel.x,firstCorner.y));
				}else{
					//end line Y = firstCorner.y (hits the top or bottom wall)
					polyPoints.add(new Point((int)((firstCorner.y-endPointYIntercept)/endPointSlope),firstCorner.y));
				}
				
				//start line X = firstCorner.x (hits one of the side walls)
				if(((startPointSlope*gameView.getWidth()) + startPointYIntercept) >= 0 && ((startPointSlope*gameView.getWidth()) + startPointYIntercept) < gameView.getHeight()){
					//if both extension points hit the same wall don't need corner
					polyPoints.add(new Point(firstCorner.x,firstCorner.y));
					if(startPointSlope == 0){
						polyPoints.add(new Point(firstCorner.x,(int)sourceCenterPixel.y));
					}else{
						polyPoints.add(new Point(firstCorner.x,(int)((startPointSlope*firstCorner.x) + startPointYIntercept)));
					}
				}else{
					if(startPointSlope == Float.POSITIVE_INFINITY || startPointSlope == Float.NEGATIVE_INFINITY){
						//Directly below or above
						polyPoints.add(new Point((int)sourceCenterPixel.x,firstCorner.y));
					}else{
						//if extension points are on different walls we need to add the corner
						polyPoints.add(new Point((int)((firstCorner.y-startPointYIntercept)/startPointSlope),firstCorner.y));
					}
				}
			}
		}else{
			//Horizontally or Vertically blocking
			
			//end line X = firstCorner.x (hits side wall)
			if(((endPointSlope*firstCorner.x) + endPointYIntercept) >= 0 && ((endPointSlope*firstCorner.x) + endPointYIntercept) < gameView.getHeight()){
				polyPoints.add(new Point(firstCorner.x,(int)(((endPointSlope*firstCorner.x) + endPointYIntercept))));
				
				
				if(((startPointSlope*secondCorner.x) + startPointYIntercept) >= 0 && ((startPointSlope*secondCorner.x) + startPointYIntercept) < gameView.getHeight()) {
					//start line X = secondCorner.x (hits side wall)
					if(firstCorner.x == secondCorner.x){
						//Same wall
						polyPoints.add(new Point(secondCorner.x,(int)(((startPointSlope*secondCorner.x) + startPointYIntercept))));
					}else{
						//Opposite walls
						polyPoints.add(firstCorner);
						polyPoints.add(secondCorner);
						polyPoints.add(new Point(secondCorner.x,(int)(((startPointSlope*secondCorner.x) + startPointYIntercept))));
					}
				}else {
					//start line Y = secondCorner.y (hits top or bottom wall)
					if(firstCorner.x == secondCorner.x){
						polyPoints.add(secondCorner);
					}else{
						polyPoints.add(firstCorner);
					}
					polyPoints.add(new Point((int)((secondCorner.y-startPointYIntercept)/startPointSlope),secondCorner.y));
				}
			}else{
				//end line Y = firstCorner.y (hits top or bottom wall)
				polyPoints.add(new Point((int)((firstCorner.y-endPointYIntercept)/endPointSlope),firstCorner.y));
				
				if(((startPointSlope*secondCorner.x) + startPointYIntercept) >= 0 && ((startPointSlope*secondCorner.x) + startPointYIntercept) < gameView.getHeight()) {
					//start line X = secondCorner.x (hits side wall)
					if(firstCorner.y == secondCorner.y) {
						polyPoints.add(secondCorner);
					}else{
						polyPoints.add(firstCorner);
					}
					polyPoints.add(new Point(secondCorner.x,(int)(((startPointSlope*secondCorner.x) + startPointYIntercept))));
				}else{
					//start line Y = secondCorner.y (hits top or bottom wall)
					if(firstCorner.y == secondCorner.y){
						//hits the same wall (no need for corners
						polyPoints.add(new Point((int)((secondCorner.y-startPointYIntercept)/startPointSlope),secondCorner.y));
					}else{
						//if extension points are on different walls we need to add the corner
						//BUG HERE: cant rely on the symetry now that we are calculating during graduated motion
						polyPoints.add(firstCorner);
						polyPoints.add(secondCorner);
						polyPoints.add(new Point((int)((secondCorner.y-startPointYIntercept)/startPointSlope),secondCorner.y));
					}
				}
			}
		}
		return calculatePath(polyPoints);
	}
	
	private void expandCells(Point destBottomLeft, Point destBottomRight, Point destTopRight){
		destBottomLeft.y++;
		destBottomRight.x++;
		destBottomRight.y++;
		destTopRight.x++;
	}
	
	/**
	 * Calculates the radial blocking shadow implemented by dest on player
	 * @param player - the player object containing current location etc
	 * @param destX - the x position of the blocking object
	 * @param destY - the y position of the blocking object
	 * @param xVision - the number of cells radially (horizontally) outward from center
	 * @param yVision - the number of cells radially (vertically) outward from center
	 * @return
	 */
	private Path calculateRadialRegion(Player player, int destX, int destY, int xVision, int yVision) {
		Point destStartPixel;
		Point destEndPixel;
		List<Point> polyPoints = new LinkedList<Point>();
		
		Point sourceCenterPixel = screen.absoluteCellToCenterPixel(new Point(player.getPosition().x,player.getPosition().y), player, gameView.getWidth(), gameView.getHeight(), false);
		Point destTopLeft = screen.absoluteCellToTopLeftPixel(new Point(destX,destY), player, gameView.getWidth(), gameView.getHeight(), true);
		Point destTopRight = screen.absoluteCellToTopRightPixel(new Point(destX,destY), player, gameView.getWidth(), gameView.getHeight(), true);
		Point destBottomLeft = screen.absoluteCellToBottomLeftPixel(new Point(destX,destY), player, gameView.getWidth(), gameView.getHeight(), true);
		Point destBottomRight = screen.absoluteCellToBottomRightPixel(new Point(destX,destY), player, gameView.getWidth(), gameView.getHeight(), true);
		Point destCenter = screen.absoluteCellToCenterPixel(new Point(destX,destY), player, gameView.getWidth(), gameView.getHeight(), true);
		
		//Find start pixel and end pixel based on their location relative to player
		//start pixel is the first pixel contacted when rotating counter clockwise
		if(destBottomRight.x <= sourceCenterPixel.x && destBottomRight.y <= sourceCenterPixel.y) {
			//Top Left or bottom right from the center cell
			//Use bottomleft and topright corners
			
			//Dirty Fix(expand the cells by one pixel down and right to fix sliver of vision through walls)
			expandCells(destBottomLeft, destBottomRight, destTopRight);
			
			destStartPixel = destTopRight;
			polyPoints.add(destStartPixel);
			polyPoints.add(destTopLeft);
			destEndPixel = destBottomLeft;
			polyPoints.add(destEndPixel);
			
			//Calculate the rest of the polygon
			return finishCalculatingPolygon(destEndPixel, destStartPixel, sourceCenterPixel, polyPoints, new Point(0,0), new Point(0,0));
			
		}else if(destTopLeft.x >= sourceCenterPixel.x && destTopLeft.y >= sourceCenterPixel.y){
			//bottom right from the center cell
			//Use bottomleft and topright corners

			//Dirty Fix(expand the cells by one pixel down and right to fix sliver of vision through walls)
			expandCells(destBottomLeft, destBottomRight, destTopRight);
			
			destStartPixel = destBottomLeft;
			polyPoints.add(destStartPixel);
			polyPoints.add(destBottomRight);
			destEndPixel = destTopRight;
			polyPoints.add(destEndPixel);
			
			//Calculate the rest of the polygon
			return finishCalculatingPolygon(destEndPixel, destStartPixel, sourceCenterPixel, polyPoints, new Point(gameView.getWidth(),gameView.getHeight()), new Point(gameView.getWidth(),gameView.getHeight()));
			
		}else if(destBottomLeft.x >= sourceCenterPixel.x && destBottomLeft.y <= sourceCenterPixel.y) {
			//Top Right from the center cell
			//Uses topleft and bottomright corners

			//Dirty Fix(expand the cells by one pixel down and right to fix sliver of vision through walls)
			expandCells(destBottomLeft, destBottomRight, destTopRight);
			
			destStartPixel = destBottomRight;
			polyPoints.add(destStartPixel);
			polyPoints.add(destTopRight);
			destEndPixel = destTopLeft;
			polyPoints.add(destEndPixel);
			
			//Calculate the rest of the polygon
			return finishCalculatingPolygon(destEndPixel, destStartPixel, sourceCenterPixel, polyPoints, new Point(gameView.getWidth(),0), new Point(gameView.getWidth(),0));
			
			
		}else if(destTopRight.x <= sourceCenterPixel.x && destTopRight.y >= sourceCenterPixel.y) {
			//bottom left from the center cell
			//Uses topleft and bottomright corners

			//Dirty Fix(expand the cells by one pixel down and right to fix sliver of vision through walls)
			expandCells(destBottomLeft, destBottomRight, destTopRight);

			destStartPixel = destTopLeft;
			polyPoints.add(destStartPixel);
			polyPoints.add(destBottomLeft);
			destEndPixel = destBottomRight;
			polyPoints.add(destEndPixel);
			
			//Calculate the rest of the polygon
			return finishCalculatingPolygon(destEndPixel, destStartPixel, sourceCenterPixel, polyPoints, new Point(0,gameView.getHeight()), new Point(0,gameView.getHeight()));
			
		}else if(destBottomLeft.x < sourceCenterPixel.x && destBottomRight.x > sourceCenterPixel.x && destCenter.y < sourceCenterPixel.y) {
			//Directly above the center cell
			//Uses bottomleft and bottomright corners
			destStartPixel = destBottomRight;
			polyPoints.add(destStartPixel);
			polyPoints.add(destTopRight);
			polyPoints.add(destTopLeft);
			destEndPixel = destBottomLeft;
			polyPoints.add(destEndPixel);
			

			//Calculate the rest of the polygon
			return finishCalculatingPolygon(destEndPixel, destStartPixel, sourceCenterPixel, polyPoints, new Point(0,0), new Point(gameView.getWidth(),0));
			
			
		}else if(destTopLeft.x < sourceCenterPixel.x && destTopRight.x > sourceCenterPixel.x && destCenter.y > sourceCenterPixel.y) {
			//Directly below the center cell
			//Uses topleft and topright corners
			destStartPixel = destTopLeft;
			polyPoints.add(destStartPixel);
			polyPoints.add(destBottomLeft);
			polyPoints.add(destBottomRight);
			destEndPixel = destTopRight;
			polyPoints.add(destEndPixel);
			

			//Calculate the rest of the polygon
			return finishCalculatingPolygon(destEndPixel, destStartPixel, sourceCenterPixel, polyPoints, new Point(gameView.getWidth(),gameView.getHeight()), new Point(0,gameView.getHeight()));
			
		}else if(destCenter.x > sourceCenterPixel.x && destTopLeft.y < sourceCenterPixel.y && destBottomLeft.y > sourceCenterPixel.y) {
			//if(destX > player.getPosition().x && destY == player.getPosition().y)
			//Directly to the right of the center cell
			//Uses topleft and bottomleft corners
			destStartPixel = destBottomLeft;
			polyPoints.add(destStartPixel);
			polyPoints.add(destBottomRight);
			polyPoints.add(destTopRight);
			destEndPixel = destTopLeft;
			polyPoints.add(destEndPixel);

			//Calculate the rest of the polygon
			return finishCalculatingPolygon(destEndPixel, destStartPixel, sourceCenterPixel, polyPoints, new Point(gameView.getWidth(),0), new Point(gameView.getWidth(),gameView.getHeight()));			
			
		}else {
			//Directly to the left of the center cell
			//Uses topright and bottomright corners
			destStartPixel = destTopRight;
			polyPoints.add(destStartPixel);
			polyPoints.add(destTopLeft);
			polyPoints.add(destBottomLeft);
			destEndPixel = destBottomRight;
			polyPoints.add(destEndPixel);

			//Calculate the rest of the polygon
			return finishCalculatingPolygon(destEndPixel, destStartPixel, sourceCenterPixel, polyPoints, new Point(0,gameView.getHeight()), new Point(0,0));

		}
	}
	
	private Path calculatePath(List<Point> polyPoints) {
		Path polyPath = new Path();
		polyPath.moveTo(polyPoints.get(0).x, polyPoints.get(0).y);
		for(int i = 0; i < polyPoints.size(); i++) {
			polyPath.lineTo(polyPoints.get(i).x, polyPoints.get(i).y);
		}
		
		return polyPath;
	}
	
	public Path getShadowPolygon(int index) {
		return notVisiblePolygons.get(index);
	}
	
	public int getNumberOfShadows() {
		return notVisiblePolygons.size();
	}
	
}
